﻿using System.ComponentModel.DataAnnotations;

namespace SendoDeo.Modelo
{
    public class Materia
    {
        public int Id { get; set; }
        [Required]
        public string Nombre { get; set; }
    }
}
