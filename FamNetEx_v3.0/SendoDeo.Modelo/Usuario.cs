﻿using System.ComponentModel.DataAnnotations;

namespace SendoDeo.Modelo
{
    public class Usuario 
    {
        public int Id { get; set; }
        [Required, MaxLength(50)]
        public string Nombre { get; set; }
        [Required, DataType(DataType.Password), MaxLength(50)]
        public string Password { get; set; }
    }
}
