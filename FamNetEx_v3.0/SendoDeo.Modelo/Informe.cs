﻿using System.ComponentModel.DataAnnotations;

namespace SendoDeo.Modelo
{
    public class Informe
    {
        public int Id { get; set; }
        [Required, MaxLength(50)]
        public string Titulo { get; set; }
        [Required, MaxLength(50)]
        public string Autor { get; set; }
        [Required, MaxLength(50)]
        public string Tutor { get; set; }
        [Display(Name = "Año"), Required, MaxLength(4)]
        public string Anio { get; set; }
    }
}
