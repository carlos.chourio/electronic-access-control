﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SendoDeo.Modelo
{
    public class NotaDeEntrega
    {
        public int Id { get; set; }
        [Required]
        public Estudiante Estudiante { get; set; }
        [Required]
        public Tesis Tesis { get; set; }
        [Required]
        public DateTime FechaDeEntrega { get; set; }
        [Required]
        public DateTime FechaDeSalida { get; set; }
    }
}
