﻿using System.ComponentModel.DataAnnotations;

namespace SendoDeo.Modelo
{
    public class Estudiante
    {
        public int Id { get; set; }
        [MaxLength(50), Required]
        public string Nombre { get; set; }
        [MaxLength(50), Required]
        public string Apellido { get; set; }
        [MaxLength(8), Required]
        public string Cedula { get; set; }
        [MaxLength(10), Required]
        public string Expediente { get; set; }
        [DataType(DataType.PhoneNumber), MaxLength(11), Required]
        public string Telefono { get; set; }
        [DataType(DataType.EmailAddress),MaxLength(50)]
        public string Correo { get; set; }
        public bool EstaDentroDeSala { get; set; }
        public bool EsAyudante { get; set; }
        public override string ToString()
        {
            return $"{Nombre} {Apellido}";
        }
    }
}