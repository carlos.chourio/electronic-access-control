﻿using System.ComponentModel.DataAnnotations;

namespace SendoDeo.Modelo
{
    public class Libro
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string Titulo { get; set; }
        [MaxLength(50)]
        public string Autor { get; set; }
        [Display(Name = "Año"), Required, MaxLength(4)]
        public string Anio { get; set; }
        [Required, MaxLength(50)]
        public string Codigo { get; set; }
        public Materia Materia { get; set; }
    }
}
