﻿using System.ComponentModel.DataAnnotations;

namespace SendoDeo.Modelo
{
    public class Tesis
    {
        public int Id { get; set; }
        [Display(Name = "Año"), Required, MaxLength(4)]
        public string Anio { get; set; }
        [MaxLength(255)]
        public string Titulo { get; set; }
        [MaxLength(50)]
        public string NombreTutor { get; set; }
        [MaxLength(50)]
        public string NombreAutores { get; set; }
        [Required, MaxLength(50)]
        public string Codigo { get; set; }
    }
}
