﻿using System;
using System.Windows.Input;

namespace SendoDeo.InterfazUsuario.Libreria
{
    public class RelayCommand<T> : ICommand
    {
        public Action<T> execute { get; private set; }

        public Func<T, bool> canExecute {get; private set;}

        public event EventHandler CanExecuteChanged;

        public RelayCommand(Action<T> execute, Func<T,bool> canExecute)
        {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return (canExecute == null || canExecute((T)parameter));
        }

        public void Execute(object parameter)
        {
            execute.Invoke((T)parameter);
        }
    }

    public class RelayCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public Action execute { get; set; }

        public Func<bool> canExecute { get; set; }

        public RelayCommand(Action execute, Func<bool> canExecute)
        {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return (canExecute == null || canExecute());
        }

        public void Execute(object parameter)
        {
            execute.Invoke();
        }
    }
}
