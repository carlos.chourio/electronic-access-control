﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SendoDeo.InterfazUsuario.Libreria
{
    public class ClaseObservable : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotificarCambioDePropiedad([CallerMemberName]string nombreDeLaPropiedad=null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nombreDeLaPropiedad));
        }
    }
}
