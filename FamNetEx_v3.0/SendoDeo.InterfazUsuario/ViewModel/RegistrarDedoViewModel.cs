﻿using SendoDeo.DataAccess;
using SendoDeo.InterfazUsuario.Libreria;
using SendoDeo.InterfazUsuario.Repositorios;
using SendoDeo.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SendoDeo.InterfazUsuario.ViewModel
{
    public class RegistrarDedoViewModel : ClaseObservable
    {
        #region Variables Privadas
        private IRepositorioGenerico<Estudiante> repositorioEstudiante;
        private SendoDeoContext context;
        private bool camposActivos;
        private bool camposVisibles;
        private string cedula;
        private string nombre;
        private string correo;
        private string telefono;
        private string apellido;
        private string expediente;
        #endregion

        public RegistrarDedoViewModel()
        {
            context = new SendoDeoContext();
            repositorioEstudiante = new RepositorioEstudiante(context);
            CamposActivos = true;
            InicializarCommands();
        } 

        private void InicializarCommands()
        {
            SiguienteCommand = new RelayCommand(SiguienteExecute, SiguienteCanExecute);
        }

        private void SiguienteExecute()
        {
            CamposActivos = false;
        }

        private bool SiguienteCanExecute()
        {
            return true;
        }

        #region Propiedades
        public string Cedula
        {
            get { return cedula; }
            set
            {
                if (cedula != value)
                {
                    int x;
                    if (string.IsNullOrEmpty(value) || int.TryParse(value, out x))
                    {
                        cedula = value;
                    }
                    else
                    {
                        MessageBox.Show("Este campo solo puede contener números", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    NotificarCambioDePropiedad();
                }
            }
        }

        public string Nombre
        {
            get { return nombre; }
            set
            {
                if (nombre != value)
                {
                    if (!ContieneNumeros(value))
                    {
                        nombre = value;
                        NotificarCambioDePropiedad();
                    }
                    else
                    {
                        MessageBox.Show("Este campo no puede contener números", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        public string Apellido
        {
            get { return apellido; }
            set
            {
                if (apellido != value)
                {
                    if (!ContieneNumeros(value))
                    {
                        apellido = value;
                        NotificarCambioDePropiedad();
                    }
                    else
                    {
                        MessageBox.Show("Este campo no puede contener números", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        public string Correo
        {
            get { return correo; }
            set
            {
                if (correo != value)
                {
                    correo = value;
                    NotificarCambioDePropiedad();
                }
            }
        }

        public string Expediente
        {
            get { return expediente; }
            set
            {
                int x;
                if (string.IsNullOrEmpty(value) || int.TryParse(value, out x))
                {
                    expediente = value;
                }
                else
                {
                    MessageBox.Show("Este campo solo puede contener números", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        public string Telefono
        {
            get { return telefono; }
            set
            {
                long x;
                if (string.IsNullOrEmpty(value) || long.TryParse(value, out x))
                {
                    telefono = value;
                }
                else
                {
                    MessageBox.Show("Este campo solo puede contener números", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        public bool CamposActivos
        {
            get { return camposActivos; }
            set
            {
                if (camposActivos != value)
                {
                    camposActivos = value;
                    NotificarCambioDePropiedad(nameof(CamposActivos));
                }
            }
        }

        public bool CamposVisibles
        {
            get { return camposVisibles; }
            set
            {
                if (camposVisibles != value)
                {
                    camposVisibles = value;
                    NotificarCambioDePropiedad(nameof(CamposVisibles));
                }

            }
        }

        public ICommand SiguienteCommand { get; set; }
        #endregion

        #region Metodos Auxiliares
        private bool ContieneNumeros(string texto)
        {
            if (string.IsNullOrEmpty(texto))
            {
                return false;
            }
            return texto.Any(t => char.IsDigit(t));
        }
        #endregion
    }
}
