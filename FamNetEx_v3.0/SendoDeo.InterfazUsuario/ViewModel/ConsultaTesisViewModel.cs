﻿using SendoDeo.DataAccess;
using SendoDeo.InterfazUsuario.Libreria;
using SendoDeo.InterfazUsuario.Repositorios;
using SendoDeo.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SendoDeo.InterfazUsuario.ViewModel
{
    public class ConsultaTesisViewModel: ClaseObservable
    {
        #region Variables Privadas
        private IRepositorioGenerico<Tesis> repositorioTesis;
        private SendoDeoContext context;
        private bool camposActivos;
        private string titulo;
        private string autor;
        private string codigo;
        private string anio;
        private string tutor;
        #endregion

        public ConsultaTesisViewModel()
        {
            context = new SendoDeoContext();
            repositorioTesis = new RepositorioTesis(context);
            InicializarCommands();
        }

        private void InicializarCommands()
        {
            SiguienteCommand = new RelayCommand(SiguienteExecute, SiguienteCanExecute);
        }

        private void SiguienteExecute()
        {
            CamposActivos = false;
        }

        private bool SiguienteCanExecute()
        {
            return true;
        }

        public bool CamposActivos
        {
            get { return camposActivos; }
            set
            {
                if (camposActivos != value)
                {
                    camposActivos = value;
                    NotificarCambioDePropiedad(nameof(CamposActivos));
                }
            }
        }

        public ICommand SiguienteCommand { get; set; }

        #region Metodos Auxiliares
        private bool ContieneNumeros(string texto)
        {
            if (string.IsNullOrEmpty(texto))
            {
                return false;
            }
            return texto.Any(t => char.IsDigit(t));
        }
        #endregion

        #region Propiedades

        public string Titulo
        {
            get { return titulo; }
            set { titulo = value; }
        }


        public string Autor
        {
            get { return autor; }
            set
            {
                if (autor != value)
                {
                    if (!ContieneNumeros(value))
                    {
                        autor = value;
                        NotificarCambioDePropiedad();
                    }
                    else
                    {
                        MessageBox.Show("Este campo no puede contener números", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }


        public string Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }

        public string Tutor
        {
            get { return tutor; }
            set
            {
                if (tutor != value)
                {
                    if (!ContieneNumeros(value))
                    {
                        tutor = value;
                        NotificarCambioDePropiedad();
                    }
                    else
                    {
                        MessageBox.Show("Este campo no puede contener números", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        public string Anio
        {
            get { return anio; }
            set
            {
                if (anio != value)
                {
                    int x;
                    if (string.IsNullOrEmpty(value) || int.TryParse(value, out x))
                    {
                        anio = value;
                        NotificarCambioDePropiedad();
                    }
                    else
                    {
                        MessageBox.Show("Este campo solo puede contener números", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }


        #endregion


    }
}
