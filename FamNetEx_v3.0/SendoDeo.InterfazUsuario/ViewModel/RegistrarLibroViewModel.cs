﻿using SendoDeo.DataAccess;
using SendoDeo.InterfazUsuario.Libreria;
using SendoDeo.InterfazUsuario.Repositorios;
using SendoDeo.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SendoDeo.InterfazUsuario.ViewModel
{
    public class RegistrarLibroViewModel : ClaseObservable
    {
        #region Variables Privadas
        private IRepositorioGenerico<Libro> repositorioLibro;
        private SendoDeoContext context;
        private bool camposActivos;
        private string titulo;
        private string autor;
        private string anio;
        private string codigo;
        #endregion

        public RegistrarLibroViewModel()
        {
            context = new SendoDeoContext();
            repositorioLibro = new RepositorioLibro(context);
            InicializarComands();
        }


        #region Propiedades

        public bool CamposActivos
        {
            get { return camposActivos; }
            set { CamposActivos = value; }
        }


        public string Titulo 
        {
            get { return titulo; }
            set { titulo = value; }
        }

        public string Autor 
        {
            get { return autor; }
            set
            {
                if (autor != value)
                {
                    if (!ContieneNumeros(value)) 
                    {
                        autor = value;
                        NotificarCambioDePropiedad();
                    } 
                    else 
                    {
                        MessageBox.Show("Este campo no puede contener números", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }


        public string Anio
        {
            get { return anio; }
            set
            {
                if (anio != value) 
                {
                    int x;
                    if (string.IsNullOrEmpty(value) || int.TryParse(value, out x)) 
                    {
                        anio = value;
                    } else {
                        MessageBox.Show("Este campo solo puede contener números", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        public string Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }

        #region Commands
        public ICommand SiguienteCommand { get; set; }
        #endregion

        #endregion 

        #region Constructor

        private void InicializarComands() 
        {
            SiguienteCommand = new RelayCommand(SiguienteExecute, SiguienteCanExecute);
        }

        #endregion

        #region Executes
        private void SiguienteExecute()
        {
            CamposActivos = false;
        }

        private bool SiguienteCanExecute()
        {
            return true;
        }
        #endregion

        #region Metodos Auxiliares
        private bool ContieneNumeros(string texto) 
        {
            if (string.IsNullOrEmpty(texto)) 
            {
                return false;
            }
            return texto.Any(t => char.IsDigit(t));
        }
        #endregion
    }
}
