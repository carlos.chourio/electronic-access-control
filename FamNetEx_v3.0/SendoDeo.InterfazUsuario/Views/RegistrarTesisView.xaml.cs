﻿using SendoDeo.InterfazUsuario.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SendoDeo.InterfazUsuario.Views
{
    /// <summary>
    /// Lógica de interacción para RegistrarTesisView.xaml
    /// </summary>
    public partial class RegistrarTesisView : UserControl
    {
        RegistrarTesisViewModel RegistrarTesisViewModel { get; set; }
        public RegistrarTesisView()
        {
            InitializeComponent();
            RegistrarTesisViewModel = new RegistrarTesisViewModel();
            DataContext = this.RegistrarTesisViewModel;
        }
    }
}
