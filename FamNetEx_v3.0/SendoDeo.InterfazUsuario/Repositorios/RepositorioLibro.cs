﻿using SendoDeo.DataAccess;
using SendoDeo.Modelo;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendoDeo.InterfazUsuario.Repositorios
{
    public class RepositorioLibro : RepositorioGenerico<Libro, SendoDeoContext>
    {
        public RepositorioLibro(SendoDeoContext context) : base(context)
        {
        }

        public override Libro ConsultarRecord(int IdRecord)
        {
            return context.Libros.Include(t => t.Materia).First(y => y.Id == IdRecord);

        }
    }
}
