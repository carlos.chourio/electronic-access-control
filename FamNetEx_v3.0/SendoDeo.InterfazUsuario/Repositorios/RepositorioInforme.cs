﻿using SendoDeo.DataAccess;
using SendoDeo.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendoDeo.InterfazUsuario.Repositorios
{
    public class RepositorioInforme : RepositorioGenerico<Informe, SendoDeoContext>, IRepositorioGenerico<Informe>
    {
        public RepositorioInforme(SendoDeoContext context) : base(context)
        {
        }
    }
}
