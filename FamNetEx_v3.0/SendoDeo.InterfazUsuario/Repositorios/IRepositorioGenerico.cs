﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SendoDeo.InterfazUsuario.Repositorios
{
    public interface IRepositorioGenerico<TModelo> where TModelo : class
    {
        Task ActualizarTablaAsync();
        void AgregarRecord(TModelo Record);
        TModelo ConsultarRecord(int IdRecord);
        void EliminarRecord(TModelo modelo);
        Task<IEnumerable<TModelo>> ObtenerTodosLosRecords();
        bool TieneCambios();
    }
}