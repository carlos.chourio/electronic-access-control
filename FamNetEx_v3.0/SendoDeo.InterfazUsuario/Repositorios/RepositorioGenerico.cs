﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendoDeo.InterfazUsuario.Repositorios
{
    public class RepositorioGenerico<TModelo, TContext> : IRepositorioGenerico<TModelo>
        where TContext : DbContext, new()
        where TModelo : class
    {
        protected TContext context;

        public RepositorioGenerico(TContext context)
        {
            this.context = context;
        }

        public virtual void AgregarRecord(TModelo Record)
        {
            context.Set<TModelo>().Add(Record);
        }

        public virtual TModelo ConsultarRecord(int IdRecord)
        {
            return context.Set<TModelo>().Find(IdRecord);
        }

        public virtual void EliminarRecord(TModelo modelo)
        {
            context.Set<TModelo>().Remove(modelo);
        }

        public virtual async Task<IEnumerable<TModelo>> ObtenerTodosLosRecords()
        {
            return await context.Set<TModelo>().ToListAsync();
        }

        public virtual async Task ActualizarTablaAsync()
        {
            await context.SaveChangesAsync();
        }

        public virtual bool TieneCambios()
        {
            return context.ChangeTracker.HasChanges();
        }
    }
}
