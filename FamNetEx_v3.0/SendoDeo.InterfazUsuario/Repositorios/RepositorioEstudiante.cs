﻿using SendoDeo.DataAccess;
using SendoDeo.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendoDeo.InterfazUsuario.Repositorios
{
    public class RepositorioEstudiante : RepositorioGenerico<Estudiante, SendoDeoContext>, IRepositorioGenerico<Estudiante>
    {
        public RepositorioEstudiante(SendoDeoContext context) : base(context)
        {
        }
    }
}
