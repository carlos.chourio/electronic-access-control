﻿using SendoDeo.DataAccess;
using SendoDeo.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendoDeo.InterfazUsuario.Repositorios
{
    public class RepositorioTesis : RepositorioGenerico<Tesis, SendoDeoContext>, IRepositorioGenerico<Tesis>
    {

        public RepositorioTesis(SendoDeoContext context) : base(context)
        {

        }
    }
}
