using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace FamNetEx
{
    public partial class FormUserList : Form
    {
        private byte[] m_pUList = null;
        private bool m_bShowUType;
        private byte m_nUserTypeOld;

        [StructLayout(LayoutKind.Sequential)]
        private struct FAMUSER
        {
            public byte GroupID;
            public byte FingerID;
            public byte UserType;
            public UInt64 UserID;
        }

        public FormUserList()
        {
            InitializeComponent();
        }

        private void FormUserList_Load(object sender, EventArgs e)
        {
            listUser.Columns.Add("Group ID", 60, HorizontalAlignment.Left);
            listUser.Columns.Add("User ID", 100, HorizontalAlignment.Left);
            listUser.Columns.Add("Finger ID", 60, HorizontalAlignment.Left);
            listUser.Columns.Add("User Type", 70, HorizontalAlignment.Left);
            listUser.Columns.Add("Status", 60, HorizontalAlignment.Left);
            listUser.Columns.Add("Security Level", 80, HorizontalAlignment.Left);

            if (m_pUList == null)
                return;

            int nTotalUser = m_pUList.Length / 12;
            textTotal.Text = nTotalUser.ToString();

            FAMUSER[] Users = new FAMUSER[nTotalUser];
            uint nIDL, nIDH;
            int i, j;

            for (i = 0; i < nTotalUser; i++)
            {
                nIDL = (uint)( m_pUList[i*12] + (m_pUList[i*12+1] << 8) + (m_pUList[i*12+2] << 16) + (m_pUList[i*12+3] << 24) ); 
                nIDH = (uint)( m_pUList[i*12 + 4] + (m_pUList[i*12+5] << 8) );
                Users[i].FingerID = m_pUList[i * 12 + 6];
                Users[i].GroupID = m_pUList[i * 12 + 7];
                Users[i].UserType = m_pUList[i * 12 + 8];
                Users[i].UserID = (UInt64)( nIDL + nIDH * 0x100000000 );
            }
            // sort the user list
            FAMUSER tmpUser;
            for (i = 0; i < nTotalUser - 1; i++)
            {
                for (j = i + 1; j < nTotalUser; j++)
                {
                    if (Users[i].UserID > Users[j].UserID)
                    {
                        tmpUser.GroupID = Users[i].GroupID;
                        tmpUser.FingerID = Users[i].FingerID;
                        tmpUser.UserID = Users[i].UserID;
                        tmpUser.UserType = Users[i].UserType;
                        Users[i].GroupID = Users[j].GroupID;
                        Users[i].FingerID = Users[j].FingerID;
                        Users[i].UserID = Users[j].UserID;
                        Users[i].UserType = Users[j].UserType;
                        Users[j].GroupID = tmpUser.GroupID;
                        Users[j].FingerID = tmpUser.FingerID;
                        Users[j].UserID = tmpUser.UserID;
                        Users[j].UserType = tmpUser.UserType;
                    }
                }
            }
            int nItem = 0;
            byte nType;
            for (i = 0; i < nTotalUser; i++)
            {
                ListViewItem item = listUser.Items.Insert(nItem, Users[i].GroupID.ToString());
                item.SubItems.Add(Users[i].UserID.ToString());
                item.SubItems.Add(Users[i].FingerID.ToString());
                nType = Users[i].UserType;
                if ( (nType & 0x04) != 0)
                    item.SubItems.Add("VIP");
                else
                    item.SubItems.Add("Ordinary");
                if ( (nType & 0x08) != 0 )
                    item.SubItems.Add("Suspend");
                else
                    item.SubItems.Add("Active");
                nType &= 0x03;
                item.SubItems.Add(nType.ToString());
                nItem++;
            }
            Users = null;

            if (m_bShowUType)
                gbChangeUserType.Visible = true;
            else
                gbChangeUserType.Visible = false;
        }

        public byte[] UserList
        {
            set { m_pUList = (byte[])value.Clone(); }
        }

        public bool IsUserTypeShow
        {
            set { m_bShowUType = value; }
        }

        public UInt64 FullUserID
        {
            get
            {
                byte nGroupID = byte.Parse(textGroupID.Text);
                byte nFingerID = (byte)comboFingerID.SelectedIndex;
                UInt64 nUserID = UInt64.Parse(textUserID.Text);
                nUserID = (UInt64)( ( (nGroupID << 24) + (nFingerID << 16) ) * 0x100000000 ) + nUserID;
                return nUserID;
            }
        }

        public byte UserType
        {
            get
            {
                byte nType = (byte)comboSecurityLevel.SelectedIndex;
                if (comboUserType.SelectedIndex == 0)   //VIP
                    nType = (byte)(nType | 0x04);
                if (comboStatus.SelectedIndex == 1)       //Suspend
                    nType = (byte)(nType | 0x08);
                return nType;
            }
        }

        public bool IsUserTypeChanged
        {
            get
            {
                if (m_nUserTypeOld == UserType)
                    return false;
                return true;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GetUserInfo();
        }

        private void listUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListView.SelectedIndexCollection indexes = listUser.SelectedIndices;
            if (indexes.Count > 0)
            {
                textGroupID.Text = listUser.Items[indexes[0]].SubItems[0].Text;
                textUserID.Text = listUser.Items[indexes[0]].SubItems[1].Text;
                comboFingerID.SelectedIndex = int.Parse(listUser.Items[indexes[0]].SubItems[2].Text);
                if (m_bShowUType)
                {
                    int nSelected = int.Parse(listUser.Items[indexes[0]].SubItems[5].Text);  //Security Level
                    comboSecurityLevel.SelectedIndex = nSelected;
                    m_nUserTypeOld = (byte)nSelected;
                    if (listUser.Items[indexes[0]].SubItems[3].Text.CompareTo("VIP") == 0)
                    {
                        comboUserType.SelectedIndex = 0;
                        m_nUserTypeOld += 0x04;
                    }
                    else
                        comboUserType.SelectedIndex = 1;
                    if (listUser.Items[indexes[0]].SubItems[4].Text.CompareTo("Suspend") == 0)
                    {
                        comboStatus.SelectedIndex = 1;
                        m_nUserTypeOld += 0x08;
                    }
                    else
                        comboStatus.SelectedIndex = 0;
                }
            }
        }

        private void listUser_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            GetUserInfo();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetUserInfo()
        {
            ListView.SelectedIndexCollection indexes = listUser.SelectedIndices;
            if (indexes.Count > 0)
            {
                textGroupID.Text = listUser.Items[indexes[0]].SubItems[0].Text;
                textUserID.Text = listUser.Items[indexes[0]].SubItems[1].Text;
                comboFingerID.SelectedIndex = int.Parse(listUser.Items[indexes[0]].SubItems[2].Text);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Please select one user", "User ID", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}