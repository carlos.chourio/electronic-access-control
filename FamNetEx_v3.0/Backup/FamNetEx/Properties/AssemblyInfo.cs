﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("FamNetEx")]
[assembly: AssemblyDescription("FS83_84 .Net Demo Program")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Futronic Technology Company Ltd.")]
[assembly: AssemblyProduct("FamNetEx")]
[assembly: AssemblyCopyright("Copyright ©  2014 Futronic")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("56ba89c1-865a-4baa-a02e-a342332dfdcf")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("3.0.0.0")]
[assembly: AssemblyFileVersion("3.0.0.0")]
