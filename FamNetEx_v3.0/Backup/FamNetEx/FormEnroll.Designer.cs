namespace FamNetEx
{
    partial class FormEnroll
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textGroupID = new System.Windows.Forms.TextBox();
            this.textUserID = new System.Windows.Forms.TextBox();
            this.comboFingerID = new System.Windows.Forms.ComboBox();
            this.comboUserType = new System.Windows.Forms.ComboBox();
            this.comboSecurityLevel = new System.Windows.Forms.ComboBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Group ID:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(47, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "User ID: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Finger ID: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(34, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "User Type: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 142);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Security Level: ";
            // 
            // textGroupID
            // 
            this.textGroupID.Location = new System.Drawing.Point(114, 26);
            this.textGroupID.MaxLength = 3;
            this.textGroupID.Name = "textGroupID";
            this.textGroupID.Size = new System.Drawing.Size(54, 20);
            this.textGroupID.TabIndex = 5;
            // 
            // textUserID
            // 
            this.textUserID.Location = new System.Drawing.Point(114, 52);
            this.textUserID.MaxLength = 12;
            this.textUserID.Name = "textUserID";
            this.textUserID.Size = new System.Drawing.Size(118, 20);
            this.textUserID.TabIndex = 6;
            // 
            // comboFingerID
            // 
            this.comboFingerID.FormattingEnabled = true;
            this.comboFingerID.Items.AddRange(new object[] {
            "0 - LF5",
            "1 - LF4",
            "2 - LF3",
            "3 - LF2",
            "4 - LF1",
            "5 - RF1",
            "6 - RF2",
            "7 - RF3",
            "8 - RF4",
            "9 - RF5"});
            this.comboFingerID.Location = new System.Drawing.Point(114, 81);
            this.comboFingerID.Name = "comboFingerID";
            this.comboFingerID.Size = new System.Drawing.Size(76, 21);
            this.comboFingerID.TabIndex = 7;
            // 
            // comboUserType
            // 
            this.comboUserType.FormattingEnabled = true;
            this.comboUserType.Items.AddRange(new object[] {
            "VIP",
            "Ordinary"});
            this.comboUserType.Location = new System.Drawing.Point(114, 112);
            this.comboUserType.Name = "comboUserType";
            this.comboUserType.Size = new System.Drawing.Size(76, 21);
            this.comboUserType.TabIndex = 8;
            // 
            // comboSecurityLevel
            // 
            this.comboSecurityLevel.FormattingEnabled = true;
            this.comboSecurityLevel.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3"});
            this.comboSecurityLevel.Location = new System.Drawing.Point(114, 142);
            this.comboSecurityLevel.Name = "comboSecurityLevel";
            this.comboSecurityLevel.Size = new System.Drawing.Size(53, 21);
            this.comboSecurityLevel.TabIndex = 9;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(48, 181);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(67, 27);
            this.btnOK.TabIndex = 10;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(168, 181);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(67, 27);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // FormEnroll
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(284, 220);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.comboSecurityLevel);
            this.Controls.Add(this.comboUserType);
            this.Controls.Add(this.comboFingerID);
            this.Controls.Add(this.textUserID);
            this.Controls.Add(this.textGroupID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormEnroll";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Enrollment Form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textGroupID;
        private System.Windows.Forms.TextBox textUserID;
        private System.Windows.Forms.ComboBox comboFingerID;
        private System.Windows.Forms.ComboBox comboUserType;
        private System.Windows.Forms.ComboBox comboSecurityLevel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
    }
}