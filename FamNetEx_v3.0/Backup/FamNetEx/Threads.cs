using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace FamNetEx
{
    public partial class MainWindow : Form
    {
        private const uint GET_IMAGE_BYTES_EACH_TIME = 1600;
        private const uint RAW_IMAGE_SIZE = 153600;    //320*480

        bool GetRawImageByLines()
        {
            uint nOffset = 0;
            byte nTimesRetry = 0;
            string strPer;
            byte[] pTempBuf = new byte[GET_IMAGE_BYTES_EACH_TIME + 20];
            uint nTotal = RAW_IMAGE_SIZE;
            uint nImgSize = 0;
            while (nTotal > 0)
            {
                nImgSize = GET_IMAGE_BYTES_EACH_TIME;
                if (nTotal < nImgSize)
                    nImgSize = nTotal;
                byte nRet = m_commFam.FamDownloadRAWImage_Size_Offset(pTempBuf, nImgSize, nOffset);
                if (nRet != 0)
                {
                    nTimesRetry++;
                    if (nTimesRetry > 2)
                    {
                        return false;
                    }
                    Thread.Sleep(20);
                    continue;
                }
                nTimesRetry = 0;
                Array.Copy(pTempBuf, 0, m_pImage, nOffset, nImgSize);
                nOffset += nImgSize;
                nTotal -= nImgSize;
                strPer = string.Format("Data receiving...{0:d}%", (nOffset * 100) / RAW_IMAGE_SIZE);
                SetMessageText(strPer);
            }
            return true;
        }

        private void CaptureThread()
        {
            byte nRet;
            EnableControl(false);
            nRet = m_commFam.PrepareConnection();
            if (nRet != 0)
            {
                SetMessageText(m_commFam.ErrorMessage);
                EnableControl(true);
                return;
            }
            SetMessageText("Put finger on the scanner.");
            while (!m_bStop)
            {
                nRet = m_commFam.FamIsFingerPresent();
                if (nRet == 0)
                    break;
                else if (nRet != FamDefs.RET_NO_IMAGE)
                {
                    SetMessageText(m_commFam.ErrorMessage);
                    m_commFam.CloseConnection();
                    EnableControl(true);
                    return;
                }
                Thread.Sleep(50);
            }
            if (m_bStop)
            {
                SetMessageText("Cancelled by user!");
            }
            else
            {
                uint nContrast = 0;
                uint nBrightness = 0;
                nRet = m_commFam.FamCaptureImage(m_bPIV, ref nContrast, ref nBrightness);
                if (nRet == 0)
                {
                    int nT1 = System.Environment.TickCount;
                    if (GetRawImageByLines())
                    {
                        m_bHasImage = true;
                        ShowImage(320, 480, m_pImage);
                        SetMessageText(string.Format("Capture successfully! T:{0:d}(ms), C:{1:d}, B:{2:d}", System.Environment.TickCount - nT1, nContrast, nBrightness));
                    }
                    else
                        SetMessageText(m_commFam.ErrorMessage);
                }
                else
                    SetMessageText(m_commFam.ErrorMessage);
            }
            m_commFam.CloseConnection();
            EnableControl(true);
        }

        private void IdentifyThread()
        {
            byte nRet;
            uint nID_L = 0;
            uint nID_H = 0;
            uint nContrast = 0;
            uint nBrightness = 0;

            EnableControl(false);
            nRet = m_commFam.PrepareConnection();
            if (nRet != 0)
            {
                SetMessageText(m_commFam.ErrorMessage);
                EnableControl(true);
                return;
            }
            SetMessageText("Put finger on the scanner.");
            while (!m_bStop)
            {
                nRet = m_commFam.FamIsFingerPresent();
                if (nRet == 0)
                    break;
                else if (nRet != FamDefs.RET_NO_IMAGE)
                {
                    SetMessageText(m_commFam.ErrorMessage);
                    m_commFam.CloseConnection();
                    EnableControl(true);
                    return;
                }
                Thread.Sleep(50);
            }
            if (m_bStop)
            {
                SetMessageText("Cancelled by user!");
            }
            else
            {
                nRet = m_commFam.FamCaptureImage(m_bPIV, ref nContrast, ref nBrightness);
                if (nRet == 0)
                {
                    if(m_bShowImage)
                    {
                        nRet = 1;
                        if (GetRawImageByLines())
                            nRet = 0;
                    }
                    if (nRet == 0)
                    {
                        if (m_bShowImage)
                            ShowImage(320, 480, m_pImage);
                        nRet = m_commFam.FamProcessImage();
                        if (nRet == 0)
                        {
                            SetMessageText("Matching......");
                            int nT1 = System.Environment.TickCount;
                            nRet = m_commFam.FamIdentify(ref nID_L, ref nID_H);
                            if (nRet == 0)
                            {
                                byte nGroupID;
                                byte nFingerID;
                                nGroupID = (byte)(nID_H >> 24);
                                nFingerID = (byte)(nID_H >> 16);
                                nID_H = nID_H & 0xFFFF;
                                UInt64 ui64UID;
                                ui64UID = (UInt64)(nID_L + (nID_H * 0x100000000));
                                SetMessageText(string.Format("GID:{0:d}, FID:{1:d}, UID:{2:d}.  Time: {3:d}(ms)", nGroupID, nFingerID, ui64UID, System.Environment.TickCount - nT1));
                            }
                            else
                                SetMessageText(m_commFam.ErrorMessage);
                        }
                        else
                            SetMessageText(m_commFam.ErrorMessage);
                    }
                }
                else
                    SetMessageText(m_commFam.ErrorMessage);
            }
            m_commFam.CloseConnection();
            EnableControl(true);
        }

        private void EnrollThread()
        {
            byte nRet = 0;
            byte ncyc;
            bool bError = false;
            uint nContrast = 0;
            uint nBrightness = 0;

            EnableControl(false);
            if (m_commFam.PrepareConnection() != 0)
            {
                SetMessageText(m_commFam.ErrorMessage);
                EnableControl(true);
                return;
            }
            ncyc = 0;
            SetProgressbar(1);
            while (!m_bStop)
            {
                SetMessageText("Put your finger on the FAM");
                while (!m_bStop)
                {
                    nRet = m_commFam.FamIsFingerPresent();
                    if (nRet == 0)
                        break;
                    else if (nRet != FamDefs.RET_NO_IMAGE)
                    {
                        SetMessageText(m_commFam.ErrorMessage);
                        bError = true;
                        break;
                    }
                    Thread.Sleep(50);
                }
                if (m_bStop || bError)
                    break;
                nRet = m_commFam.FamCaptureImage(m_bPIV, ref nContrast, ref nBrightness);
                if (nRet == 0)
                {
                    if (m_bShowImage)
                    {
                        nRet = 1;
                        if (GetRawImageByLines())
                            nRet = 0;
                    }
                    if (nRet == 0)
                    {
                        if (m_bShowImage)
                            ShowImage(320, 480, m_pImage);
                        nRet = m_commFam.FamProcessImage();
                        if (nRet == 0)
                        {
                            nRet = m_commFam.FamStoreSample(ncyc);
                            if (nRet == 0)
                            {
                                ncyc++;
                                SetProgressbar(2);
                                if (ncyc == m_nMaxSamples)
                                    break;
                            }
                            else
                            {
                                SetMessageText(m_commFam.ErrorMessage);
                                bError = true;
                                break;
                            }
                        }
                        else if (nRet == FamDefs.RET_BAD_QUALITY || nRet == FamDefs.RET_TOO_LITTLE_POINTS)
                        {
                            // 0x42 -> Bad quality 0x43->Too little points
                            SetMessageText(m_commFam.ErrorMessage);
                            Thread.Sleep(1500);
                        }
                        else
                        {
                            SetMessageText(m_commFam.ErrorMessage);
                            bError = true;
                            break;
                        }
                    }
                    else
                    {
                        SetMessageText(m_commFam.ErrorMessage);
                        bError = true;
                        break;
                    }
                }
                else
                {
                    SetMessageText(m_commFam.ErrorMessage);
                    bError = true;
                    break;
                }
                //
                SetMessageText("Take your finger off the FAM");
                while (!m_bStop)
                {
                    nRet = m_commFam.FamIsFingerPresent();
                    if (nRet == FamDefs.RET_NO_IMAGE)
                        break;
                    else if (nRet != 0)
                    {
                        SetMessageText(m_commFam.ErrorMessage);
                        bError = true;
                        break;
                    }
                    Thread.Sleep(50);
                }
            }
            if (!m_bStop && !bError)
            {
                nRet = m_commFam.FamStoreTemplate(m_nIDL, m_nIDH, m_nUserType);
                if (nRet == 0)
                {
                    SetMessageText("Enroll user successully!");
                }
                else
                    SetMessageText(m_commFam.ErrorMessage);
            }
            else if (m_bStop)
            {
                SetMessageText("Cancelled by user!");
            }
            SetProgressbar(0);
            m_commFam.CloseConnection();
            EnableControl(true);
        }

        private void VerifyThread()
        {
            byte nRet;
            uint nContrast = 0;
            uint nBrightness = 0;

            SetMessageText("Put finger on the scanner.");
            while (!m_bStop)
            {
                nRet = m_commFam.FamIsFingerPresent();
                if (nRet == 0)
                    break;
                else if (nRet != FamDefs.RET_NO_IMAGE)
                {
                    SetMessageText(m_commFam.ErrorMessage);
                    m_commFam.CloseConnection();
                    EnableControl(true);
                    return;
                }
                Thread.Sleep(50);
            }
            if (m_bStop)
            {
                SetMessageText("Cancelled by user!");
            }
            else
            {
                nRet = m_commFam.FamCaptureImage(m_bPIV, ref nContrast, ref nBrightness);
                if (nRet == 0)
                {
                    if (m_bShowImage)
                    {
                        nRet = 1;
                        if (GetRawImageByLines())
                            nRet = 0;
                    }
                    if (nRet == 0)
                    {
                        if (m_bShowImage)
                            ShowImage(320, 480, m_pImage);
                        nRet = m_commFam.FamProcessImage();
                        if (nRet == 0)
                        {
                            nRet = m_commFam.FamVerify(m_nIDL, m_nIDH);
                            if (nRet == 0)
                            {
                                SetMessageText("Verify OK!");
                            }
                            else
                                SetMessageText(m_commFam.ErrorMessage);
                        }
                        else
                            SetMessageText(m_commFam.ErrorMessage);
                    }
                    else
                        SetMessageText(m_commFam.ErrorMessage);
                }
                else
                    SetMessageText(m_commFam.ErrorMessage);
            }
            m_commFam.CloseConnection();
            EnableControl(true);
        }
    }
}
