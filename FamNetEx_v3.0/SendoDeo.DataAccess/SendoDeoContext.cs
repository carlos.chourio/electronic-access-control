﻿using SendoDeo.Modelo;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendoDeo.DataAccess
{
    public class SendoDeoContext : DbContext
    {
        public DbSet<Tesis> Tesis { get; set; }

        public DbSet<Estudiante> Estudiantes { get; set; }

        public DbSet<NotaDeEntrega> NotasDeEntrega { get; set; }

        public DbSet<Informe> Informes { get; set; }

        public DbSet<Libro> Libros { get; set; }

        public DbSet<Materia> Materias { get; set; }

        public DbSet<Usuario> Usuarios{ get; set; }

        public SendoDeoContext() : base("ConexionTesis")
        {
        }
    }
}
