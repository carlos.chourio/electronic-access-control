namespace FamNetEx
{
    partial class FormUserList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textTotal = new System.Windows.Forms.TextBox();
            this.listUser = new System.Windows.Forms.ListView();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.gbChangeUserType = new System.Windows.Forms.GroupBox();
            this.comboStatus = new System.Windows.Forms.ComboBox();
            this.comboSecurityLevel = new System.Windows.Forms.ComboBox();
            this.comboUserType = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboFingerID = new System.Windows.Forms.ComboBox();
            this.textUserID = new System.Windows.Forms.TextBox();
            this.textGroupID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.gbChangeUserType.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Please select the user";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(391, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Total:";
            // 
            // textTotal
            // 
            this.textTotal.Location = new System.Drawing.Point(431, 9);
            this.textTotal.Name = "textTotal";
            this.textTotal.ReadOnly = true;
            this.textTotal.Size = new System.Drawing.Size(64, 20);
            this.textTotal.TabIndex = 2;
            // 
            // listUser
            // 
            this.listUser.FullRowSelect = true;
            this.listUser.Location = new System.Drawing.Point(15, 35);
            this.listUser.MultiSelect = false;
            this.listUser.Name = "listUser";
            this.listUser.Size = new System.Drawing.Size(480, 265);
            this.listUser.TabIndex = 3;
            this.listUser.UseCompatibleStateImageBehavior = false;
            this.listUser.View = System.Windows.Forms.View.Details;
            this.listUser.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listUser_MouseDoubleClick);
            this.listUser.SelectedIndexChanged += new System.EventHandler(this.listUser_SelectedIndexChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(282, 416);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(77, 26);
            this.btnCancel.TabIndex = 17;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(120, 416);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(77, 26);
            this.btnOK.TabIndex = 16;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // gbChangeUserType
            // 
            this.gbChangeUserType.Controls.Add(this.comboStatus);
            this.gbChangeUserType.Controls.Add(this.comboSecurityLevel);
            this.gbChangeUserType.Controls.Add(this.comboUserType);
            this.gbChangeUserType.Controls.Add(this.label6);
            this.gbChangeUserType.Controls.Add(this.label5);
            this.gbChangeUserType.Controls.Add(this.label4);
            this.gbChangeUserType.Location = new System.Drawing.Point(17, 344);
            this.gbChangeUserType.Name = "gbChangeUserType";
            this.gbChangeUserType.Size = new System.Drawing.Size(478, 59);
            this.gbChangeUserType.TabIndex = 15;
            this.gbChangeUserType.TabStop = false;
            this.gbChangeUserType.Text = "Change User Type";
            this.gbChangeUserType.Visible = false;
            // 
            // comboStatus
            // 
            this.comboStatus.FormattingEnabled = true;
            this.comboStatus.Items.AddRange(new object[] {
            "Active",
            "Suspend"});
            this.comboStatus.Location = new System.Drawing.Point(398, 27);
            this.comboStatus.Name = "comboStatus";
            this.comboStatus.Size = new System.Drawing.Size(72, 21);
            this.comboStatus.TabIndex = 5;
            // 
            // comboSecurityLevel
            // 
            this.comboSecurityLevel.FormattingEnabled = true;
            this.comboSecurityLevel.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3"});
            this.comboSecurityLevel.Location = new System.Drawing.Point(265, 27);
            this.comboSecurityLevel.Name = "comboSecurityLevel";
            this.comboSecurityLevel.Size = new System.Drawing.Size(46, 21);
            this.comboSecurityLevel.TabIndex = 4;
            // 
            // comboUserType
            // 
            this.comboUserType.FormattingEnabled = true;
            this.comboUserType.Items.AddRange(new object[] {
            "VIP",
            "Ordinary"});
            this.comboUserType.Location = new System.Drawing.Point(71, 27);
            this.comboUserType.Name = "comboUserType";
            this.comboUserType.Size = new System.Drawing.Size(72, 21);
            this.comboUserType.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(180, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Security Level:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(352, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Status:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "User Type:";
            // 
            // comboFingerID
            // 
            this.comboFingerID.FormattingEnabled = true;
            this.comboFingerID.Items.AddRange(new object[] {
            "0 - LF5",
            "1 - LF4",
            "2 - LF3",
            "3 - LF2",
            "4 - LF1",
            "5 - RF1",
            "6 - RF2",
            "7 - RF3",
            "8 - RF4",
            "9 - RF5"});
            this.comboFingerID.Location = new System.Drawing.Point(402, 313);
            this.comboFingerID.Name = "comboFingerID";
            this.comboFingerID.Size = new System.Drawing.Size(85, 21);
            this.comboFingerID.TabIndex = 14;
            // 
            // textUserID
            // 
            this.textUserID.Location = new System.Drawing.Point(210, 313);
            this.textUserID.MaxLength = 12;
            this.textUserID.Name = "textUserID";
            this.textUserID.Size = new System.Drawing.Size(106, 20);
            this.textUserID.TabIndex = 13;
            // 
            // textGroupID
            // 
            this.textGroupID.Location = new System.Drawing.Point(88, 313);
            this.textGroupID.MaxLength = 3;
            this.textGroupID.Name = "textGroupID";
            this.textGroupID.Size = new System.Drawing.Size(47, 20);
            this.textGroupID.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(343, 316);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Finger ID:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(159, 316);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "User ID: ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(23, 316);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Group ID: ";
            // 
            // FormUserList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(507, 451);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.gbChangeUserType);
            this.Controls.Add(this.comboFingerID);
            this.Controls.Add(this.textUserID);
            this.Controls.Add(this.textGroupID);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.listUser);
            this.Controls.Add(this.textTotal);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormUserList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FAM User List";
            this.Load += new System.EventHandler(this.FormUserList_Load);
            this.gbChangeUserType.ResumeLayout(false);
            this.gbChangeUserType.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textTotal;
        private System.Windows.Forms.ListView listUser;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.GroupBox gbChangeUserType;
        private System.Windows.Forms.ComboBox comboStatus;
        private System.Windows.Forms.ComboBox comboSecurityLevel;
        private System.Windows.Forms.ComboBox comboUserType;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboFingerID;
        private System.Windows.Forms.TextBox textUserID;
        private System.Windows.Forms.TextBox textGroupID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}