using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace FamNetEx
{
    public partial class FormUserID : Form
    {
        private bool m_bShowUType;

        public FormUserID()
        {
            InitializeComponent();
            comboFingerID.SelectedIndex = 6;
            comboUserType.SelectedIndex = 0;
            comboStatus.SelectedIndex = 0;
            comboSecurityLevel.SelectedIndex = 2;
            m_bShowUType = false;
        }

        public bool IsUserTypeShow
        {
            set { m_bShowUType = value; }
        }

        public UInt64 FullUserID
        {
            get
            {
                UInt64 nUserID;
                byte nGroupID = byte.Parse(textGroupID.Text);
                byte nFingerID = (byte)comboFingerID.SelectedIndex;
                nUserID = UInt64.Parse(textUserID.Text);
                nUserID = (UInt64)(((nGroupID << 24) + (nFingerID << 16)) * 0x100000000) + nUserID;
                return nUserID;
            }
        }

        public byte UserType
        {
            get
            {
                byte nType = (byte)comboSecurityLevel.SelectedIndex;
                if (comboUserType.SelectedIndex == 0)   //VIP
                    nType = (byte)(nType | 0x04);
                if (comboStatus.SelectedIndex == 1)       //Suspend
                    nType = (byte)(nType | 0x08);
                return nType;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (textGroupID.Text.Trim() == null || textUserID.Text.Trim() == null)
                MessageBox.Show("Please enter the ID", "Parameter is required", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void FormUserID_Load(object sender, EventArgs e)
        {
            if (m_bShowUType)
                gbChangeUserType.Visible = true;
            else
                gbChangeUserType.Visible = false;
        }

     }
}