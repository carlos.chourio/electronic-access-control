using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Collections;

namespace FamNetEx
{
    public partial class MainWindow : Form
    {
        private FamComm m_commFam;
        private bool m_bStop;
        private byte[] m_pImage;
        private bool m_bHasImage = false;
        private bool m_bExit;
        private uint m_nIDL;
        private uint m_nIDH;
        private byte m_nUserType;
        private StringBuilder m_strCommandList = new StringBuilder(30*64);
        private int m_nCountOfCommandLists = 0;
        private bool m_bShowImage = true;
        private bool m_bPIV = true;
        private byte m_nMaxSamples;
        private string[] m_strComPorts;
        private byte m_nSavedInterface;
        private string m_strSavedComPort;
        /// <summary>
        /// This delegate enables asynchronous calls for setting
        /// the text property on a status control.
        /// </summary>
        /// <param name="text"></param>
        delegate void SetMessageCallback(string text);
        delegate void SetCommandTextCallback(string text);
        delegate void SetImageCallback(Bitmap hBitmap);
        delegate void SetProgressBarCallback(byte Action);  //0 - Invisible, 1 - Visible, 2 - PerformStep
        delegate void EnableControlsCallback(bool bEnable);

        public MainWindow()
        {
            InitializeComponent();
            m_bStop = false;
            m_bExit = false;
            m_pImage = new byte[153602];   //320*480+2
            progressBar1.Minimum = 0;
            progressBar1.Maximum = 3;
            progressBar1.Step = 1;
            progressBar1.Value = 0;
            m_commFam = new FamComm();
            m_commFam.Baudrate = m_commFam.MaxBaudrate = 115200;
            m_commFam.OnAddCommandList += new AddCommandListHandle(this.AddCommandList);
            m_commFam.OnShowCommandList += new ShowCommandListHandle(this.ShowCommandList);
            m_commFam.OnShowTextMessage += new ShowTextMessageHandle(this.ShowTextMessage);
            m_commFam.AddCommandEventHandle();
            m_commFam.AddTextEventHandle();
            if (picboxImage.Width > 320 && picboxImage.Height > 480)
            {
                picboxImage.Width = 320;
                picboxImage.Height = 480;
                int nWidthOffset = tbCommandList.Width - 320;
                int nHeightOffset = labelCmdList.Top - picboxImage.Bottom - 10;
                labelCmdList.Top -= nHeightOffset;
                tbCommandList.Width = picboxImage.Width;
                tbCommandList.Top -= nHeightOffset;
                tbCommandList.Height += nHeightOffset;
                if (nWidthOffset > 0)
                    this.Width -= nWidthOffset;
            }
        }

        private void LoadParameter()
        {
            string szFileName = Path.Combine(Directory.GetCurrentDirectory(), "setting.bin");
            try
            {
                using (FileStream fileStream = new FileStream(szFileName, FileMode.Open))
                {
                    UTF8Encoding utfEncoder = new UTF8Encoding();
                    byte[] Data = new byte[10];
                    if (fileStream.Length != 11)
                        throw new IOException(String.Format("Bad file {0}", fileStream.Name));
                    m_nSavedInterface = (byte)fileStream.ReadByte();
                    fileStream.Read(Data, 0, 10);
                    m_strSavedComPort = utfEncoder.GetString(Data, 0, Data.Length);
                }
            }
            catch (Exception)
            {
            }
        }

        private void SaveParameter()
        {
            string szFileName = Path.Combine(Directory.GetCurrentDirectory(), "setting.bin");
            using (FileStream fileStream = new FileStream(szFileName, FileMode.Create))
            {
                UTF8Encoding utfEncoder = new UTF8Encoding();
                byte[] Data = null;
                Data = utfEncoder.GetBytes(m_strSavedComPort);
                byte[] Data2 = new byte[10];
                Array.Copy(Data, Data2, Data.Length);
                fileStream.WriteByte(m_nSavedInterface);
                fileStream.Write(Data2, 0, Data2.Length);
            }
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            LoadParameter();

            comboInteface.SelectedIndex = m_nSavedInterface;
            comboMaxSample.SelectedIndex = 2;
            byte[] byIP = new byte[4] { 192, 168, 0, 30 };
            ipFam.SetAddressBytes(byIP);

            FamSerialComm.EnumerateComPorts();
            int nCount = FamSerialComm.FriendlyNameComPorts.Count;
            int nSelected = 0;
            if (nCount > 0)
            {
                string[] strFriendlyNames = new string[nCount];
                m_strComPorts = new string[nCount];
                FamSerialComm.FriendlyNameComPorts.Keys.CopyTo(m_strComPorts, 0);
                FamSerialComm.FriendlyNameComPorts.Values.CopyTo(strFriendlyNames, 0);
                for (int i = 0; i < nCount; i++)
                {
                    comboComPorts.Items.Add(m_strComPorts[i] + " - " + strFriendlyNames[i]);
                    if (m_strSavedComPort != null)
                        if (m_strSavedComPort.CompareTo(m_strComPorts[i]) == 0)
                            nSelected = i;
                }
            }
            comboComPorts.SelectedIndex = nSelected;
            comboBaudrate.SelectedIndex = 0;
            if (m_nSavedInterface == 1)
            {
                labelIP.Visible = false;
                labelPort.Visible = false;
                ipFam.Visible = false;
                tbPort.Visible = false;
                labelCom.Visible = true;
                labelBaudrate.Visible = true;
                comboComPorts.Visible = true;
                comboBaudrate.Visible = true;
                btnHelp.Visible = true;
                gbInterfaceSetting.Text = "Com Port";
                m_commFam.Interface = 1;
            }
        }

        private void ShowImage(int BitmapWidth, int BitmapHeight, byte[] pRawImage)
        {
            MyBitmapFile myFile = new MyBitmapFile(BitmapWidth, BitmapHeight, pRawImage);
            MemoryStream BmpStream = new MemoryStream(myFile.BitmatFileData);
            Bitmap Bmp = new Bitmap(BmpStream);
            UpdateScreenImage(Bmp);
        }

        private void AddCommandList(byte nFlag, byte[] Command)
        {
            if (m_nCountOfCommandLists > 29)    //keep maximum 30 commands
                m_strCommandList.Remove(0, 46); //each line contains fixed 46 chars

            string strCmd = string.Format("{0:X2} {1:X2} {2:X2} {3:X2} {4:X2} {5:X2} {6:X2} {7:X2} {8:X2} {9:X2} {10:X2} {11:X2} {12:X2}\r\n",
                    Command[0], Command[1], Command[2], Command[3], Command[4], Command[5], Command[6],
                    Command[7], Command[8], Command[9], Command[10], Command[11], Command[12]);
            if (nFlag == 0)
                m_strCommandList.Append("Host: " + strCmd);
            else
                m_strCommandList.Append("Fam : " + strCmd);
            if( m_nCountOfCommandLists < 30 )
                m_nCountOfCommandLists++;
        }

        private void ShowCommandList()
        {
            SetCommandText(m_strCommandList.ToString());
        }

        private void ShowTextMessage(string text)
        {
            SetMessageText(text);
        }

        private void ResetCommandList()
        {
            m_strCommandList.Remove(0, m_strCommandList.Length);
            SetCommandText("");
            m_nCountOfCommandLists = 0;
            SetMessageText("");
        }

        private void SetMessageText(string text)
        {
            // Do not change the state control during application closing.
            if (m_bExit)
                return;
            if (this.tbMessage.InvokeRequired)
            {
                SetMessageCallback d = new SetMessageCallback(this.SetMessageText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                tbMessage.Text = text;
                Update();
            }
        }

        private void SetCommandText(string text)
        {
            // Do not change the state control during application closing.
            if (m_bExit)
                return;
            if (this.tbCommandList.InvokeRequired)
            {
                SetCommandTextCallback d = new SetCommandTextCallback(this.SetCommandText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                tbCommandList.Text = text;
                Update();
            }
        }

        private void SetProgressbar(byte Action)
        {
            // Do not change the state control during application closing.
            if (m_bExit)
                return;
            if (this.progressBar1.InvokeRequired)
            {
                SetProgressBarCallback d = new SetProgressBarCallback(this.SetProgressbar);
                this.Invoke(d, new object[] { Action });
            }
            else
            {
                if (Action == 0)
                {
                    progressBar1.Value = 0;
                    progressBar1.Visible = false;
                }
                else if (Action == 1)
                    progressBar1.Visible = true;
                else
                    progressBar1.PerformStep();
            }
        }

        private void UpdateScreenImage(Bitmap hBitmap)
        {
            // Do not change the state control during application closing.
            if (m_bExit)
                return;
            if (this.picboxImage.InvokeRequired)
            {
                SetImageCallback d = new SetImageCallback(this.UpdateScreenImage);
                this.Invoke(d, new object[] { hBitmap });
            }
            else
            {
                picboxImage.Image = hBitmap;
            }
        }

        private void EnableControl(bool bEnable)
        {
            // Do not change the state control during application closing.
            if (m_bExit)
                return;
            if (this.InvokeRequired)
            {
                EnableControlsCallback d = new EnableControlsCallback(this.EnableControl);
                this.Invoke(d, new object[] { bEnable });
            }
            else
            {
                gbInterface.Enabled = bEnable;
                gbInterfaceSetting.Enabled = bEnable;
                gbSettings.Enabled = bEnable;
                gbUserManagement.Enabled = bEnable;
                btnCapture.Enabled = btnVerify.Enabled = btnEnroll.Enabled = btnIdentify.Enabled = btnPeripheralControl.Enabled = bEnable;
                comboMaxSample.Enabled = bEnable;
                checkPIV.Enabled = bEnable;
                btnSave.Enabled = (m_bHasImage && bEnable) ? true : false;
                btnCancel.Enabled = !bEnable;
            }
        }

        private void btnCapture_Click(object sender, EventArgs e)
        {
            ResetCommandList();
            m_bStop = false;
            Thread WorkerThread = new Thread(new ThreadStart(CaptureThread));
            WorkerThread.Start();
        }

        private void btnEnroll_Click(object sender, EventArgs e)
        {
            FormEnroll formE = new FormEnroll();
            if (formE.ShowDialog() == DialogResult.OK)
            {
                UInt64 ullID = UInt64.Parse(formE.UserID);
                m_nIDL = (uint)ullID;
                m_nIDH = (uint)(ullID / 0x100000000);
                m_nIDH += (uint)(formE.GroupID << 24);
                m_nIDH += (uint)(formE.FingerID << 16);
                m_nUserType = formE.UserType;
                m_bStop = false;
                ResetCommandList();
                Thread WorkerThread = new Thread(new ThreadStart(EnrollThread));
                WorkerThread.Start();
            }
            else
                SetMessageText("Cancelled by user");
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            m_bStop = true;
            m_bExit = true;
            SaveParameter();
        }

        private void btnVerify_Click(object sender, EventArgs e)
        {
            ResetCommandList();
            m_bStop = false;
            EnableControl(false);
            if (m_commFam.PrepareConnection() != 0)
            {
                SetMessageText(m_commFam.ErrorMessage);
                EnableControl(true);
                return;
            }
            // get the user id first
            if (!GetUserID(false))
            {
                EnableControl(true);
                m_commFam.CloseConnection();
                return;
            }
            // start the worker thread
            Thread WorkerThread = new Thread(new ThreadStart(VerifyThread));
            WorkerThread.Start();
        }

        private void btnIdentify_Click(object sender, EventArgs e)
        {
            ResetCommandList();
            m_bStop = false;
            Thread WorkerThread = new Thread(new ThreadStart(IdentifyThread));
            WorkerThread.Start();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            m_bStop = true;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlgSave = new SaveFileDialog();
            dlgSave.Filter = "bmp files (*.bmp)|*.bmp";
            if (dlgSave.ShowDialog() == DialogResult.OK)
            {
                MyBitmapFile myFile = new MyBitmapFile(320, 480, m_pImage);
                FileStream file = new FileStream(dlgSave.FileName, FileMode.Create);
                file.Write(myFile.BitmatFileData, 0, myFile.BitmatFileData.Length);
                file.Close();
            }
        }

        private bool GetUserID(bool bWithUserType)
        {
            //bWithUserType == true -> change the user type. Group ID and Finger ID are ignored, set to 0.
            //					and all of the same user id will be changed at the same time
            // Get user's list
            uint nListLength = 0;
            byte nRet;

            nRet = m_commFam.FamGetUserListLength(ref nListLength);
            if (nRet != 0)
            {
                if (nRet == FamDefs.RET_UNKNOWN_COMMAND)	// for FS83, command getuserlist is not supported
                {
                    FormUserID formUid = new FormUserID();
                    formUid.IsUserTypeShow = bWithUserType;
                    if( formUid.ShowDialog() == DialogResult.OK )
                    {
                        m_nIDL = (uint)formUid.FullUserID;
                        m_nIDH = (uint)(formUid.FullUserID / 0x100000000);
                        if( bWithUserType )
                            m_nUserType = formUid.UserType;
                        return true;
                    }
                    else
                    {
                        SetMessageText("Cancelled by user!");
                        return false;
                    }
                }
                else
                {
                    SetMessageText(m_commFam.ErrorMessage);
                    return false;
                }
            }
            else
            {
                if (nListLength == 0)
                {
                    SetMessageText("Empty database in FAM!");
                    return false;
                }
                byte[] pUserList = m_commFam.FamUserList;
                FormUserList formList = new FormUserList();
                formList.UserList = pUserList;
                formList.IsUserTypeShow = bWithUserType;

                if (formList.ShowDialog() == DialogResult.OK)
                {
                    if (bWithUserType && !formList.IsUserTypeChanged)
                    {
                        SetMessageText("User type is not changed!");
                        pUserList = null;
                        return false;
                    }
                    else
                    {
                        m_nIDL = (uint)formList.FullUserID;
                        m_nIDH = (uint)(formList.FullUserID / 0x100000000);
                        if (bWithUserType)
                            m_nUserType = formList.UserType;
                        pUserList = null;
                        return true;
                    }
                }
                else
                {
                    SetMessageText("Cancelled by user!");
                    pUserList = null;
                    return false;
                }
            }
        }

        private void btnChangeUser_Click(object sender, EventArgs e)
        {
            ResetCommandList();
            EnableControl(false);
            if (m_commFam.PrepareConnection() != 0)
            {
                SetMessageText(m_commFam.ErrorMessage);
                EnableControl(true);
                return;
            }
            // get the user id first
            if (!GetUserID(true))
            {
                EnableControl(true);
                m_commFam.CloseConnection();
                return;
            }
            if( m_commFam.FamChangeUserType(m_nIDL, m_nIDH, m_nUserType) == 0 )
                SetMessageText("User type is changed!");
            else
                SetMessageText(m_commFam.ErrorMessage);

            EnableControl(true);
            m_commFam.CloseConnection();
        }

        private void btnGetUser_Click(object sender, EventArgs e)
        {
            ResetCommandList();
            EnableControl(false);
	        if( m_commFam.PrepareConnection() != 0 )
	        {
                SetMessageText(m_commFam.ErrorMessage);
                EnableControl(true);
                return;
	        }
	        // get the user id first
	        if( !GetUserID( false ) )
	        {
                EnableControl(true);
		        m_commFam.CloseConnection();
		        return;
	        }

	        SetMessageText("Downloading template...");
	        uint nTemplateLength = 0;
	        if( m_commFam.FamDownloadTemplateLength(m_nIDL, m_nIDH, ref nTemplateLength) == 0 )
	        {
    	        byte[] pTemplate = m_commFam.FamDownloadedTemplate;
                SaveFileDialog dlgSave = new SaveFileDialog();
                dlgSave.Filter = "FAM template(*.fbn)|*.fbn";
                if (dlgSave.ShowDialog() == DialogResult.OK)
                {
                    FileStream file = new FileStream(dlgSave.FileName, FileMode.Create);
                    file.WriteByte( (byte)nTemplateLength );
                    file.WriteByte( (byte)(nTemplateLength >> 8) );
                    file.WriteByte( (byte)(nTemplateLength >> 16) );
                    file.WriteByte( (byte)(nTemplateLength >> 24) );
                    file.Write(pTemplate, 0, (int)nTemplateLength);
                    file.Close();
			        SetMessageText("User template is saved!");
                }
		        else
			        SetMessageText("Cancel by user!");
                pTemplate = null;
            }
	        else
                SetMessageText(m_commFam.ErrorMessage);

            EnableControl(true);
            m_commFam.CloseConnection();
        }

        private void btnSendUser_Click(object sender, EventArgs e)
        {
            ResetCommandList();
            EnableControl(false);
            if( m_commFam.PrepareConnection() != 0 )
            {
	            SetMessageText(m_commFam.ErrorMessage);
                EnableControl(true);
	            return;
            }
            OpenFileDialog dlgOpen = new OpenFileDialog();
            dlgOpen.Filter = "FAM template(*.fbn)|*.fbn";
            if (dlgOpen.ShowDialog() == DialogResult.OK)
            {
                FileStream file = new FileStream(dlgOpen.FileName, FileMode.Open, FileAccess.Read);
                int nLength = file.ReadByte() + (file.ReadByte() << 8 ) + (file.ReadByte() << 16) + (file.ReadByte() << 24);
                if( nLength > 0 )
                {
                    byte[] pTemplate = new byte[nLength+2]; // addition bytes for checksum
                    if (nLength == file.Read(pTemplate, 0, nLength))
                    {
                        if( m_commFam.FamUploadTemplate((uint)nLength, pTemplate) == 0 )
                        {
                            if( m_commFam.FamStoreTemplate(0, 0, 0x80) == 0 )
                                SetMessageText("User template is sent to FAM!");
                            else
                                SetMessageText(m_commFam.ErrorMessage);
                        }
                        else
                            SetMessageText(m_commFam.ErrorMessage);
                    }
                    else
                        SetMessageText("Invalid template file!");
                    pTemplate = null;
	            }
	            else
                    SetMessageText("Invalid template file!");
                file.Close();
            }
            else
                SetMessageText("Cancelled by user!");

            EnableControl(true);
            m_commFam.CloseConnection();
        }

        private void btnDeleteOneUser_Click(object sender, EventArgs e)
        {
            ResetCommandList();
            EnableControl(false);
            if (m_commFam.PrepareConnection() != 0)
            {
                SetMessageText(m_commFam.ErrorMessage);
                EnableControl(true);
                return;
            }
            // get the user id first
            if (!GetUserID(false))
            {
                EnableControl(true);
                m_commFam.CloseConnection();
                return;
            }
            if( m_commFam.FamDeleteOneUser(m_nIDL, m_nIDH) == 0 )
            {
                SetMessageText("User is deleted.");
            }
            else
                SetMessageText(m_commFam.ErrorMessage);

            EnableControl(true);
            m_commFam.CloseConnection();
        }

        private void btnDeleteAllUser_Click(object sender, EventArgs e)
        {
            ResetCommandList();
            EnableControl(false);
            if (m_commFam.PrepareConnection() != 0)
            {
                SetMessageText(m_commFam.ErrorMessage);
                EnableControl(true);
                return;
            } 
            if( MessageBox.Show("Are you sure to delete all the users?\n", "Delete All User", MessageBoxButtons.YesNo, MessageBoxIcon.Question ) == DialogResult.Yes )
            {
                if( m_commFam.FamDeleteAllUser() == 0 )
                    SetMessageText("All the users are deleted.");
                else
                    SetMessageText(m_commFam.ErrorMessage);
            }
            else
                SetMessageText("Cancelled by user!");

            EnableControl(true);
            m_commFam.CloseConnection();
        }

        private void btnNetworkSetting_Click(object sender, EventArgs e)
        {
            ResetCommandList();
            EnableControl(false);

            if (m_commFam.PrepareConnection() != 0)
            {
                SetMessageText(m_commFam.ErrorMessage);
                EnableControl(true);
                return;
            }

            uint nIP2Set = 0;
            uint nSM2Set = 0;
            uint nGW2Set = 0;
            byte[] arrMac = new byte[6];
            uint nPort2Set = 0;

            //Check Network setting
            if (m_commFam.FamCheckNetwork(ref nIP2Set, ref nGW2Set, ref nSM2Set, ref nPort2Set, arrMac) != 0)
            {
                SetMessageText(m_commFam.ErrorMessage);
                EnableControl(true);
                m_commFam.CloseConnection();
                return;
            }
            FormNetwork formN = new FormNetwork();
            formN.IPAddress = nIP2Set;
            formN.SubnetMask = nSM2Set;
            formN.Gateway = nGW2Set;
            formN.MacAddress = arrMac;
            formN.PortNumber = nPort2Set;
            if (formN.ShowDialog() == DialogResult.OK)
            {
                if (formN.IsSettingChanged)
                {
                    nIP2Set = formN.IPAddress;
                    nGW2Set = formN.Gateway;
                    nSM2Set = formN.SubnetMask;
                    nPort2Set = formN.PortNumber;
                    if (m_commFam.FamSetNetwork(nIP2Set, nGW2Set, nSM2Set, nPort2Set, arrMac) != 0)
                    {
                        SetMessageText(m_commFam.ErrorMessage);
                        EnableControl(true);
                        m_commFam.CloseConnection();
                        return;
                    }
                    if (MessageBox.Show("Would you like to save the change and reboot?\n\n", "Network setting", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        if (m_commFam.FamSaveNetworkSetting() != 0)
                        {
                            SetMessageText(m_commFam.ErrorMessage);
                            EnableControl(true);
                            m_commFam.CloseConnection();
                            return;
                        }
                        SetMessageText("Reboot......");
                        m_commFam.FamReboot();
                        SetMessageText("Network setting is changed successfully!");
                    }
                    else
                    {
                        SetMessageText("Network setting is NOT saved!");
                    }
                }
                else
                    SetMessageText("Network setting is not changed!");
            }
            else
            {
                SetMessageText("Cancelled by user!");
            }
            EnableControl(true);
            m_commFam.CloseConnection();
        }

        private void btUpgradeFirmware_Click(object sender, EventArgs e)
        {
            ResetCommandList();
            EnableControl(false);
	        if( m_commFam.PrepareConnection() != 0 )
	        {
		        SetMessageText(m_commFam.ErrorMessage);
                EnableControl(true);
		        return;
	        }
	        string strVerFw = "";
	        string strVerHw = "";
	        if( m_commFam.FamGetVersion(ref strVerFw, ref strVerHw) == 0)
	        {
		        string strVersion = "Current Firmware Version: " + strVerFw + ", Hardware Version: " + strVerHw + 
                    "\n\nAre you sure to upgrade the FW?\n\n";
		        if( MessageBox.Show( strVersion, "Upgrade firmware", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes )
		        {
                    OpenFileDialog dlgOpen = new OpenFileDialog();
                    dlgOpen.Filter = "BF Firmware File(*.ldr)|*.ldr";
                    if (dlgOpen.ShowDialog() == DialogResult.OK)
                    {
				        if( MessageBox.Show("Please do NOT unplug the network cable or POWER OFF the Fin'Lock device!\n\n\n", "Upgrade firmware", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK )
                            UpgradeIt( dlgOpen.FileName );
                        else
                            SetMessageText("Cancelled by user");
                    }
                }
                else
                    SetMessageText("Cancelled by user!");
	        }
	        else
	        {
		        SetMessageText(m_commFam.ErrorMessage);
	        }
            EnableControl(true);
	        m_commFam.CloseConnection();
        }

        private bool DownloadFirmwareData(bool bRamFlash, int nLength, ref byte[] RxBuf)
        {
            if (nLength <= 0)
                return false;

            uint nRxLength = 0;
            uint nAddress = 0;
            uint nTotal = (uint)nLength;
            int nRetries = 0;
            byte[] RxTemp = new byte[1030];
            string strPerMsg;
            byte nRet = 0;

            while (nTotal > 0)
            {
                nRxLength = 1024;
                if (nRxLength > nTotal)
                    nRxLength = nTotal;
                if (bRamFlash)
                    nRet = m_commFam.FamDownloadFromRam(nAddress, nRxLength, RxTemp);
                else
                    nRet = m_commFam.FamDownloadFromFlash(nAddress, nRxLength, RxTemp);
                if (nRet != 0)
                {
                    nRetries++;
                    if (nRetries > 2)
                    {
                        SetMessageText(m_commFam.ErrorMessage);
                        return false;
                    }
                    Thread.Sleep(20);
                    continue;
                }
                nRetries = 0;
                Array.Copy(RxTemp, 0, RxBuf, nAddress, nRxLength);
                nTotal -= nRxLength;
                nAddress += nRxLength;
                if (bRamFlash)
                    strPerMsg = string.Format("Downloading from RAM......{0:d}%", (nAddress * 100) / nLength);
                else
                    strPerMsg = string.Format("Downloading from Flash......{0:d}%", (nAddress * 100) / nLength);
                SetMessageText(strPerMsg);
            }
            return true;
        }

        private bool UpgradeIt(string csPath)
        {
            FileStream file = new FileStream(csPath, FileMode.Open, FileAccess.Read);
            int nLength = (int)file.Length;
            if ((nLength <= 0) || (nLength > 0x100000))
            {
                SetMessageText(string.Format("ERROR: file length = {0:d}", nLength));
                return false;
            }
            byte[] TxBuf = new byte[nLength + 12];
            byte[] RxBuf = new byte[nLength + 12];
            int i = 0;
            if (nLength != file.Read(TxBuf, 0, nLength))
            {
                SetMessageText(string.Format("ERROR: file read length is not {0:d}", nLength));
                return false;
            }
            file.Close();

            //try to download from RAM
            if (m_commFam.FamDownloadFromRam(0, 10, RxBuf) == 0)
            {
                SetMessageText("Uploading to RAM ......");
                if (m_commFam.FamUploadToRam(0, (uint)nLength, TxBuf) == 0)
                {
                    //if (m_commFam.FamDownloadFromRam(0, (uint)nLength, RxBuf) == 0)
                    if (!DownloadFirmwareData(true, nLength, ref RxBuf))
                    {
                        TxBuf = null;
                        RxBuf = null;
                        return false;
                    }
                    for (i = 0; i < nLength; i++)
                    {
                        if (RxBuf[i] != TxBuf[i])
                        {
                            SetMessageText(string.Format("{0:d} != {1:d}, Error upload/download - RAM", RxBuf[i], TxBuf[i]));
                            TxBuf = null;
                            RxBuf = null;
                            return false;
                        }
                    }
                }
                else
                {
                    SetMessageText(m_commFam.ErrorMessage);
                    TxBuf = null;
                    RxBuf = null;
                    return false;
                }
                SetMessageText(string.Format("Write to RAM {0:d} bytes.", nLength));
                Thread.Sleep(200);
                SetMessageText("Writing to Flash......");
                //---- write to flash ---------------------------------
                if (m_commFam.FamWriteToFlash((uint)nLength) != 0)
                {
                    SetMessageText(m_commFam.ErrorMessage);
                    TxBuf = null;
                    RxBuf = null;
                    return false;
                }
                SetMessageText(string.Format("Write to Flash {0:d} bytes.", nLength));
                Thread.Sleep(200);
                //-----	Verify---------------------------------------------
                if (!DownloadFirmwareData(false, nLength, ref RxBuf))
                {
                    TxBuf = null;
                    RxBuf = null;
                    return false;
                }
                // Compare
                SetMessageText("Verifying ......");
                for (i = 0; i < nLength; i++)
                {
                    if (RxBuf[i] != TxBuf[i])
                    {
                        SetMessageText(string.Format("Compare error: addr=0x{0:X}, mustbe=0x{1:X}, really=0x{2:X}", i, TxBuf[i], RxBuf[i]));
                        TxBuf = null;
                        RxBuf = null;
                        return false;
                    }
                }
                SetMessageText("Reboot, please wait ......");
                m_commFam.FamReboot();
                SetMessageText("Upgrade FAM Firmware successfully!");
            }
            else// old algoritm
            {
                SetMessageText("Old algorithm is not supported!");
                TxBuf = null;
                RxBuf = null;
                return false;
            }
            TxBuf = null;
            RxBuf = null;
            return true;
        }

        private void btnGlobalSecurityLevel_Click(object sender, EventArgs e)
        {
            ResetCommandList();
            EnableControl(false);
            if (m_commFam.PrepareConnection() != 0)
            {
                SetMessageText(m_commFam.ErrorMessage);
                EnableControl(true);
                return;
            }
            uint nPages = 0;
            if(m_commFam.FamGetSpace(ref nPages) != 0 )
            {
                SetMessageText(m_commFam.ErrorMessage);
                EnableControl(true);
                m_commFam.CloseConnection();
                return;
            }
            // get global security level
            byte nGSL = 0;
            if( m_commFam.FamGetSecurityLevel(ref nGSL) != 0 )
            {
                SetMessageText(m_commFam.ErrorMessage);
                EnableControl(true);
                m_commFam.CloseConnection();
                return;
            }

            FormOthers formO = new FormOthers();
            formO.FreePages = nPages;
            formO.GlobalSecurityLevel = nGSL;

            if( formO.ShowDialog() == DialogResult.OK )
            {
                if (formO.IsGSLChanged)
                {
                    nGSL = formO.GlobalSecurityLevel;
                    if( m_commFam.FamSetSecurityLevel(nGSL) == 0 )
                        SetMessageText("Global security level is changed!");
                    else
                        SetMessageText(m_commFam.ErrorMessage);
                }
                else
                    SetMessageText("Global security level is not changed!");
            }
            else
                SetMessageText("Cancelled by user!");
            EnableControl(true);
            m_commFam.CloseConnection();
        }

        private void btnPeripheralControl_Click(object sender, EventArgs e)
        {
            ResetCommandList();
            EnableControl(false);
            if (m_commFam.PrepareConnection() != 0)
            {
                SetMessageText(m_commFam.ErrorMessage);
                EnableControl(true);
                return;
            }
            // For Peripheral Control Form, the command list is shown in the FormPeripheral
            // need to remove the event handler in MainForm
            // after return from FormPeripheral, need to add the event handler again
            //
            m_commFam.RemoveCommandEventHandle();
            m_commFam.OnAddCommandList -= new AddCommandListHandle(this.AddCommandList);
            m_commFam.OnShowCommandList -= new ShowCommandListHandle(this.ShowCommandList);
            SetMessageText("Peripheral control.");

            FormPeripheral formP = new FormPeripheral();
            // the event handler will be added in the FormPerpheral
            formP.CommFam = m_commFam;
            formP.ShowDialog();

            EnableControl(true);
            m_commFam.CloseConnection();
            // add the event handler again
            m_commFam.OnAddCommandList += new AddCommandListHandle(this.AddCommandList);
            m_commFam.OnShowCommandList += new ShowCommandListHandle(this.ShowCommandList);
            m_commFam.AddCommandEventHandle();
        }

        private void checkShowImage_CheckedChanged(object sender, EventArgs e)
        {
            m_bShowImage = checkShowImage.Checked;
            if (!m_bShowImage)
            {
                picboxImage.Image = null;
                m_bHasImage = false;
                btnSave.Enabled = false;
            }
        }

        private void comboMaxSample_SelectedIndexChanged(object sender, EventArgs e)
        {
            m_nMaxSamples = (byte)( comboMaxSample.SelectedIndex + 1);
            progressBar1.Maximum = m_nMaxSamples;
        }

        private void ipFam_TextChanged(object sender, EventArgs e)
        {
            byte[] byIPAddress = ipFam.GetAddressBytes();
            m_commFam.IPAddress = byIPAddress;
        }

        private void comboInteface_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            {

                labelPort.Visible = false;

                tbPort.Visible = false;
                labelCom.Visible = true;
                labelBaudrate.Visible = true;
                comboComPorts.Visible = true;
                comboBaudrate.Visible = true;
                btnHelp.Visible = true;
                gbInterfaceSetting.Text = "Com Port";
                m_commFam.Interface = 1;
                m_nSavedInterface = 1;
            }
        }

        private void comboComPorts_SelectedIndexChanged(object sender, EventArgs e)
        {
            m_strSavedComPort = m_strComPorts[comboComPorts.SelectedIndex]; 
            m_commFam.ComPort = m_strComPorts[comboComPorts.SelectedIndex];            
        }

        private void comboBaudrate_SelectedIndexChanged(object sender, EventArgs e)
        {
            m_commFam.MaxBaudrate = int.Parse(comboBaudrate.Text);
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            string strMsg;
            strMsg = "FAM can support the following baudrate: \r\n 1- 115200,  2- 230400,  3- 460800,  4- 921600\r\n\r\n";
            strMsg += "After the FAM is power on, the default baudrate is set to 115200.\r\n";
            strMsg += "You can set the max baudrate in the program. And it is dependent on the PC COM Port.\r\n";
            strMsg += "For the built-in COM port of PC, the maximum baudrate is 115200.\r\n";
            strMsg += "For CP2101 USB to UART bridge controller, the maximum baudrate is 921600.\r\n";
            MessageBox.Show(strMsg, "Help", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void tbPort_TextChanged(object sender, EventArgs e)
        {
            m_commFam.PortNumber = int.Parse(tbPort.Text);
        }

        private void checkPIV_CheckedChanged(object sender, EventArgs e)
        {
            m_bPIV = checkPIV.Checked;
        }
    }

}