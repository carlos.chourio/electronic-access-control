namespace FamNetEx
{
    partial class FormPeripheral
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.radioLed2On = new System.Windows.Forms.RadioButton();
            this.radioLed2Off = new System.Windows.Forms.RadioButton();
            this.radioBuzzerOn = new System.Windows.Forms.RadioButton();
            this.radioBuzzerOff = new System.Windows.Forms.RadioButton();
            this.radioLockOn = new System.Windows.Forms.RadioButton();
            this.radioLockOff = new System.Windows.Forms.RadioButton();
            this.textSensor1 = new System.Windows.Forms.TextBox();
            this.textSensor2 = new System.Windows.Forms.TextBox();
            this.textMessage = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.radioLed1On = new System.Windows.Forms.RadioButton();
            this.radioLed1Off = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "LED1:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "LED2:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Buzzer:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Lock:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(28, 162);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Sensor1:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(28, 188);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Sensor2:";
            // 
            // radioLed2On
            // 
            this.radioLed2On.AutoSize = true;
            this.radioLed2On.Location = new System.Drawing.Point(74, 12);
            this.radioLed2On.Name = "radioLed2On";
            this.radioLed2On.Size = new System.Drawing.Size(39, 17);
            this.radioLed2On.TabIndex = 5;
            this.radioLed2On.Text = "On";
            this.radioLed2On.UseVisualStyleBackColor = true;
            this.radioLed2On.CheckedChanged += new System.EventHandler(this.radioLed2On_CheckedChanged);
            // 
            // radioLed2Off
            // 
            this.radioLed2Off.AutoSize = true;
            this.radioLed2Off.Location = new System.Drawing.Point(173, 12);
            this.radioLed2Off.Name = "radioLed2Off";
            this.radioLed2Off.Size = new System.Drawing.Size(39, 17);
            this.radioLed2Off.TabIndex = 6;
            this.radioLed2Off.Text = "Off";
            this.radioLed2Off.UseVisualStyleBackColor = true;
            this.radioLed2Off.CheckedChanged += new System.EventHandler(this.radioLed2Off_CheckedChanged);
            // 
            // radioBuzzerOn
            // 
            this.radioBuzzerOn.AutoSize = true;
            this.radioBuzzerOn.Location = new System.Drawing.Point(74, 12);
            this.radioBuzzerOn.Name = "radioBuzzerOn";
            this.radioBuzzerOn.Size = new System.Drawing.Size(39, 17);
            this.radioBuzzerOn.TabIndex = 8;
            this.radioBuzzerOn.Text = "On";
            this.radioBuzzerOn.UseVisualStyleBackColor = true;
            this.radioBuzzerOn.CheckedChanged += new System.EventHandler(this.radioBuzzerOn_CheckedChanged);
            // 
            // radioBuzzerOff
            // 
            this.radioBuzzerOff.AutoSize = true;
            this.radioBuzzerOff.Location = new System.Drawing.Point(173, 12);
            this.radioBuzzerOff.Name = "radioBuzzerOff";
            this.radioBuzzerOff.Size = new System.Drawing.Size(39, 17);
            this.radioBuzzerOff.TabIndex = 9;
            this.radioBuzzerOff.Text = "Off";
            this.radioBuzzerOff.UseVisualStyleBackColor = true;
            this.radioBuzzerOff.CheckedChanged += new System.EventHandler(this.radioBuzzerOff_CheckedChanged);
            // 
            // radioLockOn
            // 
            this.radioLockOn.AutoSize = true;
            this.radioLockOn.Location = new System.Drawing.Point(74, 12);
            this.radioLockOn.Name = "radioLockOn";
            this.radioLockOn.Size = new System.Drawing.Size(39, 17);
            this.radioLockOn.TabIndex = 11;
            this.radioLockOn.Text = "On";
            this.radioLockOn.UseVisualStyleBackColor = true;
            this.radioLockOn.CheckedChanged += new System.EventHandler(this.radioLockOn_CheckedChanged);
            // 
            // radioLockOff
            // 
            this.radioLockOff.AutoSize = true;
            this.radioLockOff.Location = new System.Drawing.Point(173, 12);
            this.radioLockOff.Name = "radioLockOff";
            this.radioLockOff.Size = new System.Drawing.Size(39, 17);
            this.radioLockOff.TabIndex = 12;
            this.radioLockOff.Text = "Off";
            this.radioLockOff.UseVisualStyleBackColor = true;
            this.radioLockOff.CheckedChanged += new System.EventHandler(this.radioLockOff_CheckedChanged);
            // 
            // textSensor1
            // 
            this.textSensor1.Location = new System.Drawing.Point(99, 159);
            this.textSensor1.Name = "textSensor1";
            this.textSensor1.ReadOnly = true;
            this.textSensor1.Size = new System.Drawing.Size(62, 20);
            this.textSensor1.TabIndex = 14;
            // 
            // textSensor2
            // 
            this.textSensor2.Location = new System.Drawing.Point(99, 185);
            this.textSensor2.Name = "textSensor2";
            this.textSensor2.ReadOnly = true;
            this.textSensor2.Size = new System.Drawing.Size(62, 20);
            this.textSensor2.TabIndex = 16;
            // 
            // textMessage
            // 
            this.textMessage.Location = new System.Drawing.Point(25, 222);
            this.textMessage.Multiline = true;
            this.textMessage.Name = "textMessage";
            this.textMessage.ReadOnly = true;
            this.textMessage.Size = new System.Drawing.Size(243, 43);
            this.textMessage.TabIndex = 17;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(113, 278);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(66, 27);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // radioLed1On
            // 
            this.radioLed1On.AutoSize = true;
            this.radioLed1On.Location = new System.Drawing.Point(74, 12);
            this.radioLed1On.Name = "radioLed1On";
            this.radioLed1On.Size = new System.Drawing.Size(39, 17);
            this.radioLed1On.TabIndex = 2;
            this.radioLed1On.Text = "On";
            this.radioLed1On.UseVisualStyleBackColor = true;
            this.radioLed1On.CheckedChanged += new System.EventHandler(this.radioLed1On_CheckedChanged);
            // 
            // radioLed1Off
            // 
            this.radioLed1Off.AutoSize = true;
            this.radioLed1Off.Location = new System.Drawing.Point(173, 12);
            this.radioLed1Off.Name = "radioLed1Off";
            this.radioLed1Off.Size = new System.Drawing.Size(39, 17);
            this.radioLed1Off.TabIndex = 3;
            this.radioLed1Off.Text = "Off";
            this.radioLed1Off.UseVisualStyleBackColor = true;
            this.radioLed1Off.CheckedChanged += new System.EventHandler(this.radioLed1Off_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioLed1Off);
            this.groupBox1.Controls.Add(this.radioLed1On);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(25, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(243, 35);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioLed2On);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.radioLed2Off);
            this.groupBox2.Location = new System.Drawing.Point(25, 48);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(243, 35);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioBuzzerOn);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.radioBuzzerOff);
            this.groupBox3.Location = new System.Drawing.Point(25, 83);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(243, 35);
            this.groupBox3.TabIndex = 20;
            this.groupBox3.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.radioLockOn);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.radioLockOff);
            this.groupBox4.Location = new System.Drawing.Point(25, 118);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(243, 35);
            this.groupBox4.TabIndex = 21;
            this.groupBox4.TabStop = false;
            // 
            // FormPeripheral
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 317);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.textMessage);
            this.Controls.Add(this.textSensor2);
            this.Controls.Add(this.textSensor1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormPeripheral";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FormPeripheral";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormPeripheral_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textSensor1;
        private System.Windows.Forms.TextBox textSensor2;
        private System.Windows.Forms.TextBox textMessage;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.RadioButton radioLed2On;
        private System.Windows.Forms.RadioButton radioLed2Off;
        private System.Windows.Forms.RadioButton radioBuzzerOn;
        private System.Windows.Forms.RadioButton radioBuzzerOff;
        private System.Windows.Forms.RadioButton radioLockOn;
        private System.Windows.Forms.RadioButton radioLockOff;
        private System.Windows.Forms.RadioButton radioLed1On;
        private System.Windows.Forms.RadioButton radioLed1Off;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
    }
}