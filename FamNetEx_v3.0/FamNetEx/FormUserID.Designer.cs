namespace FamNetEx
{
    partial class FormUserID
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textGroupID = new System.Windows.Forms.TextBox();
            this.textUserID = new System.Windows.Forms.TextBox();
            this.comboFingerID = new System.Windows.Forms.ComboBox();
            this.gbChangeUserType = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboUserType = new System.Windows.Forms.ComboBox();
            this.comboSecurityLevel = new System.Windows.Forms.ComboBox();
            this.comboStatus = new System.Windows.Forms.ComboBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.gbChangeUserType.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Group ID: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "User ID: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Finger ID:";
            // 
            // textGroupID
            // 
            this.textGroupID.Location = new System.Drawing.Point(102, 19);
            this.textGroupID.MaxLength = 3;
            this.textGroupID.Name = "textGroupID";
            this.textGroupID.Size = new System.Drawing.Size(48, 20);
            this.textGroupID.TabIndex = 3;
            // 
            // textUserID
            // 
            this.textUserID.Location = new System.Drawing.Point(102, 45);
            this.textUserID.MaxLength = 12;
            this.textUserID.Name = "textUserID";
            this.textUserID.Size = new System.Drawing.Size(117, 20);
            this.textUserID.TabIndex = 4;
            // 
            // comboFingerID
            // 
            this.comboFingerID.FormattingEnabled = true;
            this.comboFingerID.Items.AddRange(new object[] {
            "0 - LF5",
            "1 - LF4",
            "2 - LF3",
            "3 - LF2",
            "4 - LF1",
            "5 - RF1",
            "6 - RF2",
            "7 - RF3",
            "8 - RF4",
            "9 - RF5"});
            this.comboFingerID.Location = new System.Drawing.Point(102, 72);
            this.comboFingerID.Name = "comboFingerID";
            this.comboFingerID.Size = new System.Drawing.Size(83, 21);
            this.comboFingerID.TabIndex = 5;
            // 
            // gbChangeUserType
            // 
            this.gbChangeUserType.Controls.Add(this.comboStatus);
            this.gbChangeUserType.Controls.Add(this.comboSecurityLevel);
            this.gbChangeUserType.Controls.Add(this.comboUserType);
            this.gbChangeUserType.Controls.Add(this.label6);
            this.gbChangeUserType.Controls.Add(this.label5);
            this.gbChangeUserType.Controls.Add(this.label4);
            this.gbChangeUserType.Location = new System.Drawing.Point(7, 114);
            this.gbChangeUserType.Name = "gbChangeUserType";
            this.gbChangeUserType.Size = new System.Drawing.Size(286, 91);
            this.gbChangeUserType.TabIndex = 6;
            this.gbChangeUserType.TabStop = false;
            this.gbChangeUserType.Text = "Change User Type";
            this.gbChangeUserType.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "User Type:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Status:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(151, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Security Level:";
            // 
            // comboUserType
            // 
            this.comboUserType.FormattingEnabled = true;
            this.comboUserType.Items.AddRange(new object[] {
            "VIP",
            "Ordinary"});
            this.comboUserType.Location = new System.Drawing.Point(71, 27);
            this.comboUserType.Name = "comboUserType";
            this.comboUserType.Size = new System.Drawing.Size(72, 21);
            this.comboUserType.TabIndex = 3;
            // 
            // comboSecurityLevel
            // 
            this.comboSecurityLevel.FormattingEnabled = true;
            this.comboSecurityLevel.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3"});
            this.comboSecurityLevel.Location = new System.Drawing.Point(234, 27);
            this.comboSecurityLevel.Name = "comboSecurityLevel";
            this.comboSecurityLevel.Size = new System.Drawing.Size(46, 21);
            this.comboSecurityLevel.TabIndex = 4;
            // 
            // comboStatus
            // 
            this.comboStatus.FormattingEnabled = true;
            this.comboStatus.Items.AddRange(new object[] {
            "Active",
            "Suspend"});
            this.comboStatus.Location = new System.Drawing.Point(71, 54);
            this.comboStatus.Name = "comboStatus";
            this.comboStatus.Size = new System.Drawing.Size(72, 21);
            this.comboStatus.TabIndex = 5;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(50, 217);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(77, 26);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(161, 217);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(77, 26);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // FormUserID
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(298, 255);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.gbChangeUserType);
            this.Controls.Add(this.comboFingerID);
            this.Controls.Add(this.textUserID);
            this.Controls.Add(this.textGroupID);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormUserID";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "User ID";
            this.Load += new System.EventHandler(this.FormUserID_Load);
            this.gbChangeUserType.ResumeLayout(false);
            this.gbChangeUserType.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textGroupID;
        private System.Windows.Forms.TextBox textUserID;
        private System.Windows.Forms.ComboBox comboFingerID;
        private System.Windows.Forms.GroupBox gbChangeUserType;
        private System.Windows.Forms.ComboBox comboUserType;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboStatus;
        private System.Windows.Forms.ComboBox comboSecurityLevel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
    }
}