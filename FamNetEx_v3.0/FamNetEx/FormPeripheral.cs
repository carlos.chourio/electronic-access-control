using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace FamNetEx
{
    public partial class FormPeripheral : Form
    {
        uint m_uiParam1;
        uint m_uiParam2;
        byte m_nSensorState;
        FamComm m_commFam = null;
        private StringBuilder m_strCommandList = new StringBuilder(128);

        public FormPeripheral()
        {
            InitializeComponent();
            m_uiParam1 = m_uiParam2 = 0;            
        }

        public FamComm CommFam
        {
            set 
            { 
                m_commFam = value;
                m_commFam.OnAddCommandList += new AddCommandListHandle(this.AddCommandList);
                m_commFam.OnShowCommandList += new ShowCommandListHandle(this.ShowCommandList);
                m_commFam.AddCommandEventHandle();
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SetMessageText(string strMessage)
        {
            textMessage.Text = strMessage;
        }

        private void AddCommandList(byte nFlag, byte[] Command)
        {
            if (nFlag == 0)
                m_strCommandList.Append(string.Format("Fam : {0:X2} {1:X2} {2:X2} {3:X2} {4:X2} {5:X2} {6:X2} {7:X2} {8:X2} {9:X2} {10:X2} {11:X2} {12:X2}\r\n",
                    Command[0], Command[1], Command[2], Command[3], Command[4], Command[5], Command[6],
                    Command[7], Command[8], Command[9], Command[10], Command[11], Command[12]));
            else
                m_strCommandList.Append(string.Format("Host: {0:X2} {1:X2} {2:X2} {3:X2} {4:X2} {5:X2} {6:X2} {7:X2} {8:X2} {9:X2} {10:X2} {11:X2} {12:X2}\r\n",
                    Command[0], Command[1], Command[2], Command[3], Command[4], Command[5], Command[6],
                    Command[7], Command[8], Command[9], Command[10], Command[11], Command[12]));
        }

        private void ShowCommandList()
        {
            SetMessageText(m_strCommandList.ToString());
        }

        private void ResetCommandList()
        {
            m_strCommandList.Remove(0, m_strCommandList.Length);
            SetMessageText("");
        }

        private void SetSensorState()
        {
	        if( (m_nSensorState & 0x01) != 0 )
		        textSensor1.Text = ("Open");
	        else
                textSensor1.Text = ("Close");
            if( (m_nSensorState & 0x02) != 0 )
                textSensor2.Text = ("Open");
            else
                textSensor2.Text = ("Close");
        }

        private void radioLed1On_CheckedChanged(object sender, EventArgs e)
        {
            m_uiParam1 = m_uiParam1 | 0x00000008;
            m_uiParam2 = m_uiParam2 & 0xfffffff7;
            ResetCommandList();
            if (m_commFam.FamPeripherialControl(m_uiParam1, m_uiParam2, ref m_nSensorState) != 0)
                SetMessageText(m_commFam.ErrorMessage);
            else
                SetSensorState();
        }

        private void radioLed1Off_CheckedChanged(object sender, EventArgs e)
        {
            m_uiParam2 = m_uiParam2 | 0x00000008;
            m_uiParam1 = m_uiParam1 & 0xfffffff7;
            ResetCommandList();
            if (m_commFam.FamPeripherialControl(m_uiParam1, m_uiParam2, ref m_nSensorState) != 0)
                SetMessageText(m_commFam.ErrorMessage);
            else
                SetSensorState();
        }

        private void radioLed2On_CheckedChanged(object sender, EventArgs e)
        {
            m_uiParam1 = m_uiParam1 | 0x00000010;
            m_uiParam2 = m_uiParam2 & 0xffffffef;
            ResetCommandList();
            if (m_commFam.FamPeripherialControl(m_uiParam1, m_uiParam2, ref m_nSensorState) != 0)
                SetMessageText(m_commFam.ErrorMessage);
            else
                SetSensorState();
        }

        private void radioLed2Off_CheckedChanged(object sender, EventArgs e)
        {
            m_uiParam2 = m_uiParam2 | 0x00000010;
            m_uiParam1 = m_uiParam1 & 0xffffffef;
            ResetCommandList();
            if (m_commFam.FamPeripherialControl(m_uiParam1, m_uiParam2, ref m_nSensorState) != 0)
                SetMessageText(m_commFam.ErrorMessage);
            else
                SetSensorState();
        }

        private void radioBuzzerOn_CheckedChanged(object sender, EventArgs e)
        {
            m_uiParam1 = m_uiParam1 | 0x00000001;
            m_uiParam2 = m_uiParam2 & 0xfffffffe;
            ResetCommandList();
            if (m_commFam.FamPeripherialControl(m_uiParam1, m_uiParam2, ref m_nSensorState) != 0)
                SetMessageText(m_commFam.ErrorMessage);
            else
                SetSensorState();
        }

        private void radioBuzzerOff_CheckedChanged(object sender, EventArgs e)
        {
            m_uiParam2 = m_uiParam2 | 0x00000001;
            m_uiParam1 = m_uiParam1 & 0xfffffffe;
            ResetCommandList();
            if (m_commFam.FamPeripherialControl(m_uiParam1, m_uiParam2, ref m_nSensorState) != 0)
                SetMessageText(m_commFam.ErrorMessage);
            else
                SetSensorState();
        }

        private void radioLockOn_CheckedChanged(object sender, EventArgs e)
        {
            m_uiParam1 = m_uiParam1 | 0x00000004;
            m_uiParam2 = m_uiParam2 & 0xfffffffb;
            ResetCommandList();
            if (m_commFam.FamPeripherialControl(m_uiParam1, m_uiParam2, ref m_nSensorState) != 0)
                SetMessageText(m_commFam.ErrorMessage);
            else
                SetSensorState();
        }

        private void radioLockOff_CheckedChanged(object sender, EventArgs e)
        {
            m_uiParam2 = m_uiParam2 | 0x00000004;
            m_uiParam1 = m_uiParam1 & 0xfffffffb;
            ResetCommandList();
            if (m_commFam.FamPeripherialControl(m_uiParam1, m_uiParam2, ref m_nSensorState) != 0)
                SetMessageText(m_commFam.ErrorMessage);
            else
                SetSensorState();
        }

        private void FormPeripheral_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (m_commFam != null)
            {
                m_commFam.RemoveCommandEventHandle();
                m_commFam.OnAddCommandList -= new AddCommandListHandle(this.AddCommandList);
                m_commFam.OnShowCommandList -= new ShowCommandListHandle(this.ShowCommandList);
            }
        }
    }
}