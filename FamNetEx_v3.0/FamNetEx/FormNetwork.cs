using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace FamNetEx
{
    public partial class FormNetwork : Form
    {
        private uint m_nIpAddr;
        private uint m_nSubnetMask;
        private uint m_nGateway;
        private byte[] m_MacAddr;
        private uint m_nPortNumber;
        private uint m_nIpAddr2;
        private uint m_nSubnetMask2;
        private uint m_nGateway2;
        private uint m_nPortNumber2;

        public FormNetwork()
        {
            InitializeComponent();
            m_nIpAddr = m_nSubnetMask = m_nGateway = 0;
        }

        private void FormNetwork_Load(object sender, EventArgs e)
        {
            if( m_nIpAddr > 0 )
            {
                byte[] byIpAddr = new byte[4];
                byIpAddr[3] = (byte)m_nIpAddr;
                byIpAddr[2] = (byte)(m_nIpAddr >> 8);
                byIpAddr[1] = (byte)(m_nIpAddr >> 16);
                byIpAddr[0] = (byte)(m_nIpAddr >> 24);
                ctrlIP.SetAddressBytes(byIpAddr);
            }
            if (m_nSubnetMask > 0)
            {
                byte[] bySM = new byte[4];
                bySM[3] = (byte)m_nSubnetMask;
                bySM[2] = (byte)(m_nSubnetMask >> 8);
                bySM[1] = (byte)(m_nSubnetMask >> 16);
                bySM[0] = (byte)(m_nSubnetMask >> 24);
                ctrlSubnetMask.SetAddressBytes(bySM);
            }
            if (m_nGateway > 0)
            {
                byte[] byGW = new byte[4];
                byGW[3] = (byte)m_nGateway;
                byGW[2] = (byte)(m_nGateway >> 8);
                byGW[1] = (byte)(m_nGateway >> 16);
                byGW[0] = (byte)(m_nGateway >> 24);
                ctrlGateway.SetAddressBytes(byGW);
            }
            textPort.Text = m_nPortNumber.ToString();
            textMac1.Text = string.Format("{0:X2}",m_MacAddr[0]);
            textMac2.Text = string.Format("{0:X2}", m_MacAddr[1]);
            textMac3.Text = string.Format("{0:X2}", m_MacAddr[2]);
            textMac4.Text = string.Format("{0:X2}", m_MacAddr[3]);
            textMac5.Text = string.Format("{0:X2}", m_MacAddr[4]);
            textMac6.Text = string.Format("{0:X2}", m_MacAddr[5]);
        }

        public uint IPAddress
        {
            get { return m_nIpAddr; }
            set { m_nIpAddr = m_nIpAddr2 = value; }
        }

        public uint SubnetMask
        {
            get { return m_nSubnetMask; }
            set { m_nSubnetMask = m_nSubnetMask2 = value; }
        }

        public uint Gateway
        {
            get { return m_nGateway; }
            set { m_nGateway = m_nGateway2 = value; }
        }

        public byte[] MacAddress
        {
            set { m_MacAddr = (byte[])value.Clone(); }
        }

        public uint PortNumber
        {
            get { return m_nPortNumber; }
            set { m_nPortNumber = m_nPortNumber2 = value; }
        }

        public bool IsSettingChanged
        {
            get
            {
                if (m_nIpAddr != m_nIpAddr2 || m_nSubnetMask != m_nSubnetMask2 ||
                    m_nGateway != m_nGateway2 || m_nPortNumber != m_nPortNumber2)
                    return true;
                else
                    return false;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (textPort.Text.Trim().Length == 0)
            {
                MessageBox.Show("Port nubmer is required", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            m_nPortNumber = uint.Parse(textPort.Text);
            byte[] byIP = ctrlIP.GetAddressBytes();
            m_nIpAddr = (uint)(byIP[3] + (byIP[2] << 8) + (byIP[1] << 16) + (byIP[0] << 24));
            byIP = ctrlSubnetMask.GetAddressBytes();
            m_nSubnetMask = (uint)(byIP[3] + (byIP[2] << 8) + (byIP[1] << 16) + (byIP[0] << 24));
            byIP = ctrlGateway.GetAddressBytes();
            m_nGateway = (uint)(byIP[3] + (byIP[2] << 8) + (byIP[1] << 16) + (byIP[0] << 24));
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

    }
}