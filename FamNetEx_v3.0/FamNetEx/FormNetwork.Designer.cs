namespace FamNetEx
{
    partial class FormNetwork
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ctrlIP = new IPAddressControlLib.IPAddressControl();
            this.ctrlSubnetMask = new IPAddressControlLib.IPAddressControl();
            this.ctrlGateway = new IPAddressControlLib.IPAddressControl();
            this.textMac1 = new System.Windows.Forms.TextBox();
            this.textMac2 = new System.Windows.Forms.TextBox();
            this.textMac3 = new System.Windows.Forms.TextBox();
            this.textMac4 = new System.Windows.Forms.TextBox();
            this.textMac5 = new System.Windows.Forms.TextBox();
            this.textMac6 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textPort = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "IP Address:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Subnet Mask:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(36, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Gateway:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "MAC Address:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 170);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Port Number:";
            // 
            // ctrlIP
            // 
            this.ctrlIP.AllowInternalTab = false;
            this.ctrlIP.AutoHeight = true;
            this.ctrlIP.BackColor = System.Drawing.SystemColors.Window;
            this.ctrlIP.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ctrlIP.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ctrlIP.Location = new System.Drawing.Point(104, 31);
            this.ctrlIP.MinimumSize = new System.Drawing.Size(87, 20);
            this.ctrlIP.Name = "ctrlIP";
            this.ctrlIP.ReadOnly = false;
            this.ctrlIP.Size = new System.Drawing.Size(129, 20);
            this.ctrlIP.TabIndex = 5;
            this.ctrlIP.Text = "...";
            // 
            // ctrlSubnetMask
            // 
            this.ctrlSubnetMask.AllowInternalTab = false;
            this.ctrlSubnetMask.AutoHeight = true;
            this.ctrlSubnetMask.BackColor = System.Drawing.SystemColors.Window;
            this.ctrlSubnetMask.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ctrlSubnetMask.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ctrlSubnetMask.Location = new System.Drawing.Point(104, 61);
            this.ctrlSubnetMask.MinimumSize = new System.Drawing.Size(87, 20);
            this.ctrlSubnetMask.Name = "ctrlSubnetMask";
            this.ctrlSubnetMask.ReadOnly = false;
            this.ctrlSubnetMask.Size = new System.Drawing.Size(129, 20);
            this.ctrlSubnetMask.TabIndex = 6;
            this.ctrlSubnetMask.Text = "...";
            // 
            // ctrlGateway
            // 
            this.ctrlGateway.AllowInternalTab = false;
            this.ctrlGateway.AutoHeight = true;
            this.ctrlGateway.BackColor = System.Drawing.SystemColors.Window;
            this.ctrlGateway.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ctrlGateway.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ctrlGateway.Location = new System.Drawing.Point(104, 93);
            this.ctrlGateway.MinimumSize = new System.Drawing.Size(87, 20);
            this.ctrlGateway.Name = "ctrlGateway";
            this.ctrlGateway.ReadOnly = false;
            this.ctrlGateway.Size = new System.Drawing.Size(129, 20);
            this.ctrlGateway.TabIndex = 7;
            this.ctrlGateway.Text = "...";
            // 
            // textMac1
            // 
            this.textMac1.Location = new System.Drawing.Point(104, 131);
            this.textMac1.Name = "textMac1";
            this.textMac1.ReadOnly = true;
            this.textMac1.Size = new System.Drawing.Size(22, 20);
            this.textMac1.TabIndex = 8;
            // 
            // textMac2
            // 
            this.textMac2.Location = new System.Drawing.Point(138, 131);
            this.textMac2.Name = "textMac2";
            this.textMac2.ReadOnly = true;
            this.textMac2.Size = new System.Drawing.Size(22, 20);
            this.textMac2.TabIndex = 9;
            // 
            // textMac3
            // 
            this.textMac3.Location = new System.Drawing.Point(174, 131);
            this.textMac3.Name = "textMac3";
            this.textMac3.ReadOnly = true;
            this.textMac3.Size = new System.Drawing.Size(22, 20);
            this.textMac3.TabIndex = 10;
            // 
            // textMac4
            // 
            this.textMac4.Location = new System.Drawing.Point(211, 131);
            this.textMac4.Name = "textMac4";
            this.textMac4.ReadOnly = true;
            this.textMac4.Size = new System.Drawing.Size(22, 20);
            this.textMac4.TabIndex = 11;
            // 
            // textMac5
            // 
            this.textMac5.Location = new System.Drawing.Point(247, 131);
            this.textMac5.Name = "textMac5";
            this.textMac5.ReadOnly = true;
            this.textMac5.Size = new System.Drawing.Size(22, 20);
            this.textMac5.TabIndex = 12;
            // 
            // textMac6
            // 
            this.textMac6.Location = new System.Drawing.Point(285, 131);
            this.textMac6.Name = "textMac6";
            this.textMac6.ReadOnly = true;
            this.textMac6.Size = new System.Drawing.Size(22, 20);
            this.textMac6.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(127, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(10, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "-";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(162, 134);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(10, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "-";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(198, 134);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(10, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "-";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(236, 134);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(10, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "-";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(272, 134);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(10, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "-";
            // 
            // textPort
            // 
            this.textPort.Location = new System.Drawing.Point(104, 167);
            this.textPort.MaxLength = 5;
            this.textPort.Name = "textPort";
            this.textPort.Size = new System.Drawing.Size(56, 20);
            this.textPort.TabIndex = 19;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(73, 217);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(64, 27);
            this.btnOK.TabIndex = 20;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(182, 217);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(64, 27);
            this.btnCancel.TabIndex = 21;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // FormNetwork
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 258);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.textPort);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textMac6);
            this.Controls.Add(this.textMac5);
            this.Controls.Add(this.textMac4);
            this.Controls.Add(this.textMac3);
            this.Controls.Add(this.textMac2);
            this.Controls.Add(this.textMac1);
            this.Controls.Add(this.ctrlGateway);
            this.Controls.Add(this.ctrlSubnetMask);
            this.Controls.Add(this.ctrlIP);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormNetwork";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Network Settings";
            this.Load += new System.EventHandler(this.FormNetwork_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private IPAddressControlLib.IPAddressControl ctrlIP;
        private IPAddressControlLib.IPAddressControl ctrlSubnetMask;
        private IPAddressControlLib.IPAddressControl ctrlGateway;
        private System.Windows.Forms.TextBox textMac1;
        private System.Windows.Forms.TextBox textMac2;
        private System.Windows.Forms.TextBox textMac3;
        private System.Windows.Forms.TextBox textMac4;
        private System.Windows.Forms.TextBox textMac5;
        private System.Windows.Forms.TextBox textMac6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textPort;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
    }
}