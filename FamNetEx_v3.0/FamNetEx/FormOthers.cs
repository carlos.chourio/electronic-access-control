using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace FamNetEx
{
    public partial class FormOthers : Form
    {
        private uint m_nFreePages;
        private byte m_nGSL;
        private byte m_nGSL2;

        public FormOthers()
        {
            InitializeComponent();
            m_nFreePages = m_nGSL = m_nGSL2 =0;
        }

        public uint FreePages
        {
            set { m_nFreePages = value; }
        }

        public byte GlobalSecurityLevel
        {
            get { return m_nGSL; }
            set { m_nGSL = m_nGSL2 = value; }
        }

        public bool IsGSLChanged
        {
            get { return ((m_nGSL == m_nGSL2) ? false : true); }
        }

        private void FormOthers_Load(object sender, EventArgs e)
        {
            comboGSL.SelectedIndex = (int)m_nGSL;
            textFreePages.Text = m_nFreePages.ToString();
            btnOK.Focus();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            m_nGSL = (byte)comboGSL.SelectedIndex;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}