using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace FamNetEx
{
    public class FamSocketComm
    {
        public event AddCommandListHandle OnAddCommandList;
        public event ShowCommandListHandle OnShowCommandList;

        private byte[] m_byIPAddress; // = "192.168.0.30";
        private int m_nPortNumber = 5001;
        private Socket m_sckFam;
        private uint m_nRxDataLength = 0;
        private byte[] m_pRxDataBuffer;
        private int m_nWinSockErrorCode;

        public FamSocketComm()
        {
            m_nWinSockErrorCode = 0;
            m_sckFam = null;
        }

        public byte[] IPAddress
        {
            get { return (byte[])m_byIPAddress.Clone(); }
            set { m_byIPAddress = (byte[])value.Clone(); }
        }

        public int PortNumber
        {
            get { return m_nPortNumber; }
            set { m_nPortNumber = value; }
        }

        public byte PrepareSocket()
        {
            m_sckFam = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            if (m_sckFam == null)
                return FamDefs.RET_WINSOCK_ERROR;
            //IPAddress[] IPs = Dns.GetHostAddresses(m_strIPAddress);
            IPAddress ipFam = new IPAddress(m_byIPAddress);
            m_sckFam.Blocking = false;
            try
            {
                m_sckFam.Connect(ipFam, m_nPortNumber);
            }
            catch (SocketException e)
            {
                if (e.ErrorCode == 10035)   //WSAEWOULDBLOCK
                {
                    bool bRet;
                    try
                    {
                        bRet = m_sckFam.Poll(2000000, SelectMode.SelectWrite);    //2sec timeout
                    }
                    catch
                    {
                        m_nWinSockErrorCode = e.ErrorCode;
                        m_sckFam.Close();
                        return FamDefs.RET_WINSOCK_ERROR;
                    }
                    if (bRet)
                        return 0;
                    else
                        return FamDefs.RET_CONNECT_TIMEOUT;
                }
                m_nWinSockErrorCode = e.ErrorCode;
                m_sckFam.Close();
            }
            return FamDefs.RET_WINSOCK_ERROR;
        }

        public void CloseSocket()
        {
            m_sckFam.Close();
        }

        public byte CommunicateWithFAC(byte nCommand, uint param1, uint param2, byte nFlag, byte[] RxCmd, byte[] TxBuf, byte[] RxBuf)
        {
	        byte[] CommandBuf = new byte[13]{ 0x40,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x0d }; 
            uint nChksum = 0;
	        uint nIndex;
	        int nRecv;
            int nSent;
            bool bRet = true;

	        CommandBuf[1] = nCommand;
            CommandBuf[2] = (byte)(param1 & 0xff);
            CommandBuf[3] = (byte)(param1 >> 8);
            CommandBuf[4] = (byte)(param1 >> 16);
            CommandBuf[5] = (byte)(param1 >> 24);
            CommandBuf[6] = (byte)(param2);
            CommandBuf[7] = (byte)(param2 >> 8);
            CommandBuf[8] = (byte)(param2 >> 16);
            CommandBuf[9] = (byte)(param2 >> 24);
	        CommandBuf[10] = nFlag;
	        for( nIndex=0; nIndex<11; nIndex++)
		        nChksum += CommandBuf[nIndex];
	        CommandBuf[11] = (byte) ( nChksum & 0xff );

            OnAddCommandList(0, CommandBuf);
            try
            {
                nSent = m_sckFam.Send(CommandBuf);
            }
            catch(SocketException e)
            {
                m_nWinSockErrorCode = e.ErrorCode;
                return FamDefs.RET_WINSOCK_ERROR;
            }
            if( nSent != 13 )
                return FamDefs.RET_WINSOCK_ERROR;

	        if( TxBuf != null && TxBuf.Length > 0 )
	        {
		        //checksum of data
		        nChksum = 0;
		        for( nIndex=0; nIndex<param2; nIndex++ )
			        nChksum += TxBuf[nIndex];
		        TxBuf[param2] = (byte) ( nChksum ); 	
		        param2++;
                try
                {
                    nSent = m_sckFam.Send(TxBuf, (int)param2, SocketFlags.None);
                }
                catch (SocketException e)
                {
                    m_nWinSockErrorCode = e.ErrorCode;
                    return FamDefs.RET_WINSOCK_ERROR;
                }
                if( nSent != param2 )
			        return FamDefs.RET_WINSOCK_ERROR;
	        }
	        // receive data
            try
            {
                bRet = m_sckFam.Poll(10000000, SelectMode.SelectRead);    //10sec timeout
            }
            catch (SocketException e)
            {
                m_nWinSockErrorCode = e.ErrorCode;
                return FamDefs.RET_WINSOCK_ERROR;
            }
            if( !bRet )
                return FamDefs.RET_RXD_TIMEOUT;
	        // recv data
            try
            {
                nRecv = m_sckFam.Receive(RxCmd, 13, SocketFlags.None);
            }
            catch(SocketException e)
            {
                m_nWinSockErrorCode = e.ErrorCode;
                return FamDefs.RET_WINSOCK_ERROR;
            }
	        if (nRecv <= 0)
                return FamDefs.RET_WINSOCK_ERROR;
	        //
	        OnAddCommandList(1, RxCmd);
	        OnShowCommandList();

            if (RxCmd[10] == FamDefs.RET_OK)
	        {
                if (nCommand == FamDefs.COMMAND_DOWNLOAD_RAW_IMAGE || nCommand == FamDefs.COMMAND_DOWNLOAD_FROM_FLASH
                    || nCommand == FamDefs.COMMAND_DOWNLOAD_FROM_RAM)
		        {
			        nRecv = ReceiveAll(m_sckFam, RxBuf, param2+2 );
			        if (nRecv != 0)
                        return (byte)nRecv;
		        }
                else if (nCommand == FamDefs.COMMAND_DOWNLOAD_USER_LIST || nCommand == FamDefs.COMMAND_DOWNLOAD_TEMPLATE)
		        {
			        //memcpy( &m_nRxDataLength, RxCmd+6, 4 );
                    m_nRxDataLength = (uint)( RxCmd[6] + (RxCmd[7] << 8) + (RxCmd[8] << 16) + (RxCmd[9] << 24) );
			        if( m_nRxDataLength > 0 )
			        {
				        if( m_pRxDataBuffer != null )
				        {
					        m_pRxDataBuffer = null;
				        }
				        m_pRxDataBuffer = new byte[m_nRxDataLength + 2];
                        nRecv = ReceiveAll(m_sckFam, m_pRxDataBuffer, m_nRxDataLength + 2);
				        if( nRecv != 0 )
                            return (byte)nRecv;
			        }
		        }
		        return 0;
	        }

	        if( RxCmd[10] == 0 )
                return FamDefs.RET_FLAG_ZERO;
	        else
		        return RxCmd[10];
        }

        public int WinSockErrorCode
        {
            get { return m_nWinSockErrorCode; }
        }

        public uint DataBufferLength
        {
            get { return m_nRxDataLength; }
        }

        public byte[] DataBuffer
        {
            get { return (byte[]) (m_pRxDataBuffer.Clone()); }
        }

        private int ReceiveAll(Socket s, byte[] buf, uint len)
        {
            int total=0;
            int bytesleft = (int)len;
            int n=0;
            byte[] szMsg = new byte[1] { 0 };
            bool bRet = true;

            try
            {
                bRet = m_sckFam.Poll(10000000, SelectMode.SelectRead);    //10sec timeout
            }
            catch (SocketException e)
            {
                m_nWinSockErrorCode = e.ErrorCode;
                return FamDefs.RET_WINSOCK_ERROR;
            }
            if( !bRet )
                return FamDefs.RET_RXD_TIMEOUT;

            while (total < len)
            {
                try
                {
                    n = s.Receive(buf, total, bytesleft, 0);
                }
                catch (SocketException e)
                {
                    m_nWinSockErrorCode = e.ErrorCode;
                    return FamDefs.RET_WINSOCK_ERROR;
                }
	            if( n <= 0 )
		            break;
	            total += n;
	            bytesleft -= n;
	            if( total < len )
	            {
		            // Send 1byte to FAM for acknowledge, FAM will skip this byte if don't wait input data
                    try
                    {
                        s.Send(szMsg);
                        if (!m_sckFam.Poll(10000000, SelectMode.SelectRead))    //10sec timeout
                            return FamDefs.RET_RXD_TIMEOUT;
                    }
                    catch (SocketException e)
                    {
                        m_nWinSockErrorCode = e.ErrorCode;
                        return FamDefs.RET_WINSOCK_ERROR;
                    }
                }
            }
            return n<=0 ? -1 : 0;
        }        
    }
}
