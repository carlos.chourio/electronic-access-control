namespace FamNetEx
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.gbInterface = new System.Windows.Forms.GroupBox();
            this.comboInteface = new System.Windows.Forms.ComboBox();
            this.gbInterfaceSetting = new System.Windows.Forms.GroupBox();
            this.ipFam = new IPAddressControlLib.IPAddressControl();
            this.btnHelp = new System.Windows.Forms.Button();
            this.comboBaudrate = new System.Windows.Forms.ComboBox();
            this.labelBaudrate = new System.Windows.Forms.Label();
            this.comboComPorts = new System.Windows.Forms.ComboBox();
            this.labelCom = new System.Windows.Forms.Label();
            this.tbPort = new System.Windows.Forms.TextBox();
            this.labelPort = new System.Windows.Forms.Label();
            this.labelIP = new System.Windows.Forms.Label();
            this.gbSettings = new System.Windows.Forms.GroupBox();
            this.btnGlobalSecurityLevel = new System.Windows.Forms.Button();
            this.btUpgradeFirmware = new System.Windows.Forms.Button();
            this.btnNetworkSetting = new System.Windows.Forms.Button();
            this.gbOperations = new System.Windows.Forms.GroupBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnPeripheralControl = new System.Windows.Forms.Button();
            this.comboMaxSample = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkShowImage = new System.Windows.Forms.CheckBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnIdentify = new System.Windows.Forms.Button();
            this.btnEnroll = new System.Windows.Forms.Button();
            this.btnVerify = new System.Windows.Forms.Button();
            this.btnCapture = new System.Windows.Forms.Button();
            this.gbUserManagement = new System.Windows.Forms.GroupBox();
            this.btnDeleteAllUser = new System.Windows.Forms.Button();
            this.btnDeleteOneUser = new System.Windows.Forms.Button();
            this.btnSendUser = new System.Windows.Forms.Button();
            this.btnGetUser = new System.Windows.Forms.Button();
            this.btnChangeUser = new System.Windows.Forms.Button();
            this.tbMessage = new System.Windows.Forms.TextBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.picboxImage = new System.Windows.Forms.PictureBox();
            this.labelCmdList = new System.Windows.Forms.Label();
            this.tbCommandList = new System.Windows.Forms.TextBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.checkPIV = new System.Windows.Forms.CheckBox();
            this.gbInterface.SuspendLayout();
            this.gbInterfaceSetting.SuspendLayout();
            this.gbSettings.SuspendLayout();
            this.gbOperations.SuspendLayout();
            this.gbUserManagement.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxImage)).BeginInit();
            this.SuspendLayout();
            // 
            // gbInterface
            // 
            this.gbInterface.Controls.Add(this.comboInteface);
            this.gbInterface.Location = new System.Drawing.Point(16, 15);
            this.gbInterface.Margin = new System.Windows.Forms.Padding(4);
            this.gbInterface.Name = "gbInterface";
            this.gbInterface.Padding = new System.Windows.Forms.Padding(4);
            this.gbInterface.Size = new System.Drawing.Size(285, 60);
            this.gbInterface.TabIndex = 0;
            this.gbInterface.TabStop = false;
            this.gbInterface.Text = "Interface";
            // 
            // comboInteface
            // 
            this.comboInteface.FormattingEnabled = true;
            this.comboInteface.Items.AddRange(new object[] {
            "TCP/IP",
            "COM Port"});
            this.comboInteface.Location = new System.Drawing.Point(8, 23);
            this.comboInteface.Margin = new System.Windows.Forms.Padding(4);
            this.comboInteface.Name = "comboInteface";
            this.comboInteface.Size = new System.Drawing.Size(261, 24);
            this.comboInteface.TabIndex = 0;
            this.comboInteface.SelectedIndexChanged += new System.EventHandler(this.comboInteface_SelectedIndexChanged);
            // 
            // gbInterfaceSetting
            // 
            this.gbInterfaceSetting.Controls.Add(this.ipFam);
            this.gbInterfaceSetting.Controls.Add(this.btnHelp);
            this.gbInterfaceSetting.Controls.Add(this.comboBaudrate);
            this.gbInterfaceSetting.Controls.Add(this.labelBaudrate);
            this.gbInterfaceSetting.Controls.Add(this.comboComPorts);
            this.gbInterfaceSetting.Controls.Add(this.labelCom);
            this.gbInterfaceSetting.Controls.Add(this.tbPort);
            this.gbInterfaceSetting.Controls.Add(this.labelPort);
            this.gbInterfaceSetting.Controls.Add(this.labelIP);
            this.gbInterfaceSetting.Location = new System.Drawing.Point(16, 79);
            this.gbInterfaceSetting.Margin = new System.Windows.Forms.Padding(4);
            this.gbInterfaceSetting.Name = "gbInterfaceSetting";
            this.gbInterfaceSetting.Padding = new System.Windows.Forms.Padding(4);
            this.gbInterfaceSetting.Size = new System.Drawing.Size(285, 106);
            this.gbInterfaceSetting.TabIndex = 1;
            this.gbInterfaceSetting.TabStop = false;
            this.gbInterfaceSetting.Text = "IP && Port";
            // 
            // ipFam
            // 
            this.ipFam.AllowInternalTab = false;
            this.ipFam.AutoHeight = true;
            this.ipFam.BackColor = System.Drawing.SystemColors.Window;
            this.ipFam.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ipFam.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ipFam.Location = new System.Drawing.Point(101, 33);
            this.ipFam.Margin = new System.Windows.Forms.Padding(4);
            this.ipFam.MinimumSize = new System.Drawing.Size(114, 22);
            this.ipFam.Name = "ipFam";
            this.ipFam.ReadOnly = false;
            this.ipFam.Size = new System.Drawing.Size(157, 22);
            this.ipFam.TabIndex = 9;
            this.ipFam.Text = "...";
            this.ipFam.TextChanged += new System.EventHandler(this.ipFam_TextChanged);
            // 
            // btnHelp
            // 
            this.btnHelp.Location = new System.Drawing.Point(248, 71);
            this.btnHelp.Margin = new System.Windows.Forms.Padding(4);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(25, 25);
            this.btnHelp.TabIndex = 8;
            this.btnHelp.Text = "?";
            this.btnHelp.UseVisualStyleBackColor = true;
            this.btnHelp.Visible = false;
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // comboBaudrate
            // 
            this.comboBaudrate.FormattingEnabled = true;
            this.comboBaudrate.Items.AddRange(new object[] {
            "115200",
            "230400",
            "460800",
            "921600"});
            this.comboBaudrate.Location = new System.Drawing.Point(131, 71);
            this.comboBaudrate.Margin = new System.Windows.Forms.Padding(4);
            this.comboBaudrate.Name = "comboBaudrate";
            this.comboBaudrate.Size = new System.Drawing.Size(99, 24);
            this.comboBaudrate.TabIndex = 7;
            this.comboBaudrate.Visible = false;
            this.comboBaudrate.SelectedIndexChanged += new System.EventHandler(this.comboBaudrate_SelectedIndexChanged);
            // 
            // labelBaudrate
            // 
            this.labelBaudrate.AutoSize = true;
            this.labelBaudrate.Location = new System.Drawing.Point(9, 75);
            this.labelBaudrate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelBaudrate.Name = "labelBaudrate";
            this.labelBaudrate.Size = new System.Drawing.Size(99, 17);
            this.labelBaudrate.TabIndex = 6;
            this.labelBaudrate.Text = "Max Baudrate:";
            this.labelBaudrate.Visible = false;
            // 
            // comboComPorts
            // 
            this.comboComPorts.FormattingEnabled = true;
            this.comboComPorts.Location = new System.Drawing.Point(12, 43);
            this.comboComPorts.Margin = new System.Windows.Forms.Padding(4);
            this.comboComPorts.Name = "comboComPorts";
            this.comboComPorts.Size = new System.Drawing.Size(257, 24);
            this.comboComPorts.TabIndex = 5;
            this.comboComPorts.Visible = false;
            this.comboComPorts.SelectedIndexChanged += new System.EventHandler(this.comboComPorts_SelectedIndexChanged);
            // 
            // labelCom
            // 
            this.labelCom.AutoSize = true;
            this.labelCom.Location = new System.Drawing.Point(8, 21);
            this.labelCom.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelCom.Name = "labelCom";
            this.labelCom.Size = new System.Drawing.Size(139, 17);
            this.labelCom.TabIndex = 4;
            this.labelCom.Text = "Select the COM port:";
            this.labelCom.Visible = false;
            // 
            // tbPort
            // 
            this.tbPort.Location = new System.Drawing.Point(101, 69);
            this.tbPort.Margin = new System.Windows.Forms.Padding(4);
            this.tbPort.MaxLength = 5;
            this.tbPort.Name = "tbPort";
            this.tbPort.Size = new System.Drawing.Size(64, 22);
            this.tbPort.TabIndex = 3;
            this.tbPort.Text = "5001";
            this.tbPort.TextChanged += new System.EventHandler(this.tbPort_TextChanged);
            // 
            // labelPort
            // 
            this.labelPort.AutoSize = true;
            this.labelPort.Location = new System.Drawing.Point(8, 73);
            this.labelPort.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPort.Name = "labelPort";
            this.labelPort.Size = new System.Drawing.Size(92, 17);
            this.labelPort.TabIndex = 2;
            this.labelPort.Text = "Port Number:";
            // 
            // labelIP
            // 
            this.labelIP.AutoSize = true;
            this.labelIP.Location = new System.Drawing.Point(8, 37);
            this.labelIP.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelIP.Name = "labelIP";
            this.labelIP.Size = new System.Drawing.Size(80, 17);
            this.labelIP.TabIndex = 0;
            this.labelIP.Text = "IP Address:";
            // 
            // gbSettings
            // 
            this.gbSettings.Controls.Add(this.btnGlobalSecurityLevel);
            this.gbSettings.Controls.Add(this.btUpgradeFirmware);
            this.gbSettings.Controls.Add(this.btnNetworkSetting);
            this.gbSettings.Location = new System.Drawing.Point(17, 188);
            this.gbSettings.Margin = new System.Windows.Forms.Padding(4);
            this.gbSettings.Name = "gbSettings";
            this.gbSettings.Padding = new System.Windows.Forms.Padding(4);
            this.gbSettings.Size = new System.Drawing.Size(283, 126);
            this.gbSettings.TabIndex = 2;
            this.gbSettings.TabStop = false;
            this.gbSettings.Text = "Settings";
            // 
            // btnGlobalSecurityLevel
            // 
            this.btnGlobalSecurityLevel.Location = new System.Drawing.Point(37, 87);
            this.btnGlobalSecurityLevel.Margin = new System.Windows.Forms.Padding(4);
            this.btnGlobalSecurityLevel.Name = "btnGlobalSecurityLevel";
            this.btnGlobalSecurityLevel.Size = new System.Drawing.Size(201, 28);
            this.btnGlobalSecurityLevel.TabIndex = 2;
            this.btnGlobalSecurityLevel.Text = "Global Security Level";
            this.btnGlobalSecurityLevel.UseVisualStyleBackColor = true;
            this.btnGlobalSecurityLevel.Click += new System.EventHandler(this.btnGlobalSecurityLevel_Click);
            // 
            // btUpgradeFirmware
            // 
            this.btUpgradeFirmware.Location = new System.Drawing.Point(37, 53);
            this.btUpgradeFirmware.Margin = new System.Windows.Forms.Padding(4);
            this.btUpgradeFirmware.Name = "btUpgradeFirmware";
            this.btUpgradeFirmware.Size = new System.Drawing.Size(201, 28);
            this.btUpgradeFirmware.TabIndex = 1;
            this.btUpgradeFirmware.Text = "Upgrade Firmware";
            this.btUpgradeFirmware.UseVisualStyleBackColor = true;
            this.btUpgradeFirmware.Click += new System.EventHandler(this.btUpgradeFirmware_Click);
            // 
            // btnNetworkSetting
            // 
            this.btnNetworkSetting.Location = new System.Drawing.Point(37, 20);
            this.btnNetworkSetting.Margin = new System.Windows.Forms.Padding(4);
            this.btnNetworkSetting.Name = "btnNetworkSetting";
            this.btnNetworkSetting.Size = new System.Drawing.Size(201, 28);
            this.btnNetworkSetting.TabIndex = 0;
            this.btnNetworkSetting.Text = "Network Setting";
            this.btnNetworkSetting.UseVisualStyleBackColor = true;
            this.btnNetworkSetting.Click += new System.EventHandler(this.btnNetworkSetting_Click);
            // 
            // gbOperations
            // 
            this.gbOperations.Controls.Add(this.checkPIV);
            this.gbOperations.Controls.Add(this.btnSave);
            this.gbOperations.Controls.Add(this.btnPeripheralControl);
            this.gbOperations.Controls.Add(this.comboMaxSample);
            this.gbOperations.Controls.Add(this.label1);
            this.gbOperations.Controls.Add(this.checkShowImage);
            this.gbOperations.Controls.Add(this.btnCancel);
            this.gbOperations.Controls.Add(this.btnIdentify);
            this.gbOperations.Controls.Add(this.btnEnroll);
            this.gbOperations.Controls.Add(this.btnVerify);
            this.gbOperations.Controls.Add(this.btnCapture);
            this.gbOperations.Location = new System.Drawing.Point(19, 318);
            this.gbOperations.Margin = new System.Windows.Forms.Padding(4);
            this.gbOperations.Name = "gbOperations";
            this.gbOperations.Padding = new System.Windows.Forms.Padding(4);
            this.gbOperations.Size = new System.Drawing.Size(280, 214);
            this.gbOperations.TabIndex = 3;
            this.gbOperations.TabStop = false;
            this.gbOperations.Text = "Operations";
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(21, 86);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(112, 27);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnPeripheralControl
            // 
            this.btnPeripheralControl.Location = new System.Drawing.Point(21, 178);
            this.btnPeripheralControl.Margin = new System.Windows.Forms.Padding(4);
            this.btnPeripheralControl.Name = "btnPeripheralControl";
            this.btnPeripheralControl.Size = new System.Drawing.Size(232, 27);
            this.btnPeripheralControl.TabIndex = 11;
            this.btnPeripheralControl.Text = "Peripheral Control";
            this.btnPeripheralControl.UseVisualStyleBackColor = true;
            this.btnPeripheralControl.Click += new System.EventHandler(this.btnPeripheralControl_Click);
            // 
            // comboMaxSample
            // 
            this.comboMaxSample.FormattingEnabled = true;
            this.comboMaxSample.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboMaxSample.Location = new System.Drawing.Point(196, 145);
            this.comboMaxSample.Margin = new System.Windows.Forms.Padding(4);
            this.comboMaxSample.Name = "comboMaxSample";
            this.comboMaxSample.Size = new System.Drawing.Size(55, 24);
            this.comboMaxSample.TabIndex = 7;
            this.comboMaxSample.SelectedIndexChanged += new System.EventHandler(this.comboMaxSample_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 149);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Max samples in template: ";
            // 
            // checkShowImage
            // 
            this.checkShowImage.AutoSize = true;
            this.checkShowImage.Checked = true;
            this.checkShowImage.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkShowImage.Location = new System.Drawing.Point(16, 122);
            this.checkShowImage.Margin = new System.Windows.Forms.Padding(4);
            this.checkShowImage.Name = "checkShowImage";
            this.checkShowImage.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkShowImage.Size = new System.Drawing.Size(173, 21);
            this.checkShowImage.TabIndex = 5;
            this.checkShowImage.Text = "Show image on screen";
            this.checkShowImage.UseVisualStyleBackColor = true;
            this.checkShowImage.CheckedChanged += new System.EventHandler(this.checkShowImage_CheckedChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.Enabled = false;
            this.btnCancel.Location = new System.Drawing.Point(141, 86);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(112, 27);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnIdentify
            // 
            this.btnIdentify.Location = new System.Drawing.Point(141, 54);
            this.btnIdentify.Margin = new System.Windows.Forms.Padding(4);
            this.btnIdentify.Name = "btnIdentify";
            this.btnIdentify.Size = new System.Drawing.Size(112, 27);
            this.btnIdentify.TabIndex = 3;
            this.btnIdentify.Text = "Identify";
            this.btnIdentify.UseVisualStyleBackColor = true;
            this.btnIdentify.Click += new System.EventHandler(this.btnIdentify_Click);
            // 
            // btnEnroll
            // 
            this.btnEnroll.Location = new System.Drawing.Point(21, 54);
            this.btnEnroll.Margin = new System.Windows.Forms.Padding(4);
            this.btnEnroll.Name = "btnEnroll";
            this.btnEnroll.Size = new System.Drawing.Size(112, 27);
            this.btnEnroll.TabIndex = 2;
            this.btnEnroll.Text = "Enroll";
            this.btnEnroll.UseVisualStyleBackColor = true;
            this.btnEnroll.Click += new System.EventHandler(this.btnEnroll_Click);
            // 
            // btnVerify
            // 
            this.btnVerify.Location = new System.Drawing.Point(141, 22);
            this.btnVerify.Margin = new System.Windows.Forms.Padding(4);
            this.btnVerify.Name = "btnVerify";
            this.btnVerify.Size = new System.Drawing.Size(112, 27);
            this.btnVerify.TabIndex = 1;
            this.btnVerify.Text = "Verify";
            this.btnVerify.UseVisualStyleBackColor = true;
            this.btnVerify.Click += new System.EventHandler(this.btnVerify_Click);
            // 
            // btnCapture
            // 
            this.btnCapture.Location = new System.Drawing.Point(21, 22);
            this.btnCapture.Margin = new System.Windows.Forms.Padding(4);
            this.btnCapture.Name = "btnCapture";
            this.btnCapture.Size = new System.Drawing.Size(112, 27);
            this.btnCapture.TabIndex = 0;
            this.btnCapture.Text = "Capture";
            this.btnCapture.UseVisualStyleBackColor = true;
            this.btnCapture.Click += new System.EventHandler(this.btnCapture_Click);
            // 
            // gbUserManagement
            // 
            this.gbUserManagement.Controls.Add(this.btnDeleteAllUser);
            this.gbUserManagement.Controls.Add(this.btnDeleteOneUser);
            this.gbUserManagement.Controls.Add(this.btnSendUser);
            this.gbUserManagement.Controls.Add(this.btnGetUser);
            this.gbUserManagement.Controls.Add(this.btnChangeUser);
            this.gbUserManagement.Location = new System.Drawing.Point(17, 535);
            this.gbUserManagement.Margin = new System.Windows.Forms.Padding(4);
            this.gbUserManagement.Name = "gbUserManagement";
            this.gbUserManagement.Padding = new System.Windows.Forms.Padding(4);
            this.gbUserManagement.Size = new System.Drawing.Size(280, 122);
            this.gbUserManagement.TabIndex = 4;
            this.gbUserManagement.TabStop = false;
            this.gbUserManagement.Text = "User Management";
            // 
            // btnDeleteAllUser
            // 
            this.btnDeleteAllUser.Location = new System.Drawing.Point(140, 85);
            this.btnDeleteAllUser.Margin = new System.Windows.Forms.Padding(4);
            this.btnDeleteAllUser.Name = "btnDeleteAllUser";
            this.btnDeleteAllUser.Size = new System.Drawing.Size(115, 27);
            this.btnDeleteAllUser.TabIndex = 4;
            this.btnDeleteAllUser.Text = "Delete All User";
            this.btnDeleteAllUser.UseVisualStyleBackColor = true;
            this.btnDeleteAllUser.Click += new System.EventHandler(this.btnDeleteAllUser_Click);
            // 
            // btnDeleteOneUser
            // 
            this.btnDeleteOneUser.Location = new System.Drawing.Point(19, 85);
            this.btnDeleteOneUser.Margin = new System.Windows.Forms.Padding(4);
            this.btnDeleteOneUser.Name = "btnDeleteOneUser";
            this.btnDeleteOneUser.Size = new System.Drawing.Size(115, 27);
            this.btnDeleteOneUser.TabIndex = 3;
            this.btnDeleteOneUser.Text = "Delete 1 User";
            this.btnDeleteOneUser.UseVisualStyleBackColor = true;
            this.btnDeleteOneUser.Click += new System.EventHandler(this.btnDeleteOneUser_Click);
            // 
            // btnSendUser
            // 
            this.btnSendUser.Location = new System.Drawing.Point(140, 55);
            this.btnSendUser.Margin = new System.Windows.Forms.Padding(4);
            this.btnSendUser.Name = "btnSendUser";
            this.btnSendUser.Size = new System.Drawing.Size(115, 27);
            this.btnSendUser.TabIndex = 2;
            this.btnSendUser.Text = "Send User";
            this.btnSendUser.UseVisualStyleBackColor = true;
            this.btnSendUser.Click += new System.EventHandler(this.btnSendUser_Click);
            // 
            // btnGetUser
            // 
            this.btnGetUser.Location = new System.Drawing.Point(19, 55);
            this.btnGetUser.Margin = new System.Windows.Forms.Padding(4);
            this.btnGetUser.Name = "btnGetUser";
            this.btnGetUser.Size = new System.Drawing.Size(115, 27);
            this.btnGetUser.TabIndex = 1;
            this.btnGetUser.Text = "Get User";
            this.btnGetUser.UseVisualStyleBackColor = true;
            this.btnGetUser.Click += new System.EventHandler(this.btnGetUser_Click);
            // 
            // btnChangeUser
            // 
            this.btnChangeUser.Location = new System.Drawing.Point(21, 23);
            this.btnChangeUser.Margin = new System.Windows.Forms.Padding(4);
            this.btnChangeUser.Name = "btnChangeUser";
            this.btnChangeUser.Size = new System.Drawing.Size(232, 27);
            this.btnChangeUser.TabIndex = 0;
            this.btnChangeUser.Text = "Change User Type";
            this.btnChangeUser.UseVisualStyleBackColor = true;
            this.btnChangeUser.Click += new System.EventHandler(this.btnChangeUser_Click);
            // 
            // tbMessage
            // 
            this.tbMessage.Location = new System.Drawing.Point(19, 662);
            this.tbMessage.Margin = new System.Windows.Forms.Padding(4);
            this.tbMessage.Multiline = true;
            this.tbMessage.Name = "tbMessage";
            this.tbMessage.ReadOnly = true;
            this.tbMessage.Size = new System.Drawing.Size(276, 53);
            this.tbMessage.TabIndex = 5;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(20, 722);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(4);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(276, 28);
            this.progressBar1.TabIndex = 6;
            this.progressBar1.Visible = false;
            // 
            // picboxImage
            // 
            this.picboxImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picboxImage.Location = new System.Drawing.Point(319, 23);
            this.picboxImage.Margin = new System.Windows.Forms.Padding(4);
            this.picboxImage.Name = "picboxImage";
            this.picboxImage.Size = new System.Drawing.Size(426, 590);
            this.picboxImage.TabIndex = 7;
            this.picboxImage.TabStop = false;
            // 
            // labelCmdList
            // 
            this.labelCmdList.AutoSize = true;
            this.labelCmdList.Location = new System.Drawing.Point(315, 626);
            this.labelCmdList.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelCmdList.Name = "labelCmdList";
            this.labelCmdList.Size = new System.Drawing.Size(96, 17);
            this.labelCmdList.TabIndex = 8;
            this.labelCmdList.Text = "Command list:";
            // 
            // tbCommandList
            // 
            this.tbCommandList.Location = new System.Drawing.Point(319, 647);
            this.tbCommandList.Margin = new System.Windows.Forms.Padding(4);
            this.tbCommandList.Multiline = true;
            this.tbCommandList.Name = "tbCommandList";
            this.tbCommandList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbCommandList.Size = new System.Drawing.Size(425, 141);
            this.tbCommandList.TabIndex = 9;
            // 
            // btnExit
            // 
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnExit.Location = new System.Drawing.Point(68, 756);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(163, 33);
            this.btnExit.TabIndex = 10;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // checkPIV
            // 
            this.checkPIV.AutoSize = true;
            this.checkPIV.Checked = true;
            this.checkPIV.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkPIV.Location = new System.Drawing.Point(215, 122);
            this.checkPIV.Name = "checkPIV";
            this.checkPIV.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkPIV.Size = new System.Drawing.Size(51, 21);
            this.checkPIV.TabIndex = 13;
            this.checkPIV.Text = "PIV";
            this.checkPIV.UseVisualStyleBackColor = true;
            this.checkPIV.CheckedChanged += new System.EventHandler(this.checkPIV_CheckedChanged);
            // 
            // MainWindow
            // 
            this.AcceptButton = this.btnExit;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 796);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.tbCommandList);
            this.Controls.Add(this.labelCmdList);
            this.Controls.Add(this.picboxImage);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.tbMessage);
            this.Controls.Add(this.gbUserManagement);
            this.Controls.Add(this.gbOperations);
            this.Controls.Add(this.gbSettings);
            this.Controls.Add(this.gbInterfaceSetting);
            this.Controls.Add(this.gbInterface);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Futronic FAM .Net Example V3.0";
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.gbInterface.ResumeLayout(false);
            this.gbInterfaceSetting.ResumeLayout(false);
            this.gbInterfaceSetting.PerformLayout();
            this.gbSettings.ResumeLayout(false);
            this.gbOperations.ResumeLayout(false);
            this.gbOperations.PerformLayout();
            this.gbUserManagement.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picboxImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbInterface;
        private System.Windows.Forms.ComboBox comboInteface;
        private System.Windows.Forms.GroupBox gbInterfaceSetting;
        private System.Windows.Forms.Label labelIP;
        private System.Windows.Forms.TextBox tbPort;
        private System.Windows.Forms.Label labelPort;
        private System.Windows.Forms.Label labelCom;
        private System.Windows.Forms.ComboBox comboComPorts;
        private System.Windows.Forms.ComboBox comboBaudrate;
        private System.Windows.Forms.Label labelBaudrate;
        private System.Windows.Forms.Button btnHelp;
        private System.Windows.Forms.GroupBox gbSettings;
        private System.Windows.Forms.Button btnGlobalSecurityLevel;
        private System.Windows.Forms.Button btUpgradeFirmware;
        private System.Windows.Forms.Button btnNetworkSetting;
        private System.Windows.Forms.GroupBox gbOperations;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnIdentify;
        private System.Windows.Forms.Button btnEnroll;
        private System.Windows.Forms.Button btnVerify;
        private System.Windows.Forms.Button btnCapture;
        private System.Windows.Forms.CheckBox checkShowImage;
        private System.Windows.Forms.ComboBox comboMaxSample;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPeripheralControl;
        private System.Windows.Forms.GroupBox gbUserManagement;
        private System.Windows.Forms.Button btnDeleteAllUser;
        private System.Windows.Forms.Button btnDeleteOneUser;
        private System.Windows.Forms.Button btnSendUser;
        private System.Windows.Forms.Button btnGetUser;
        private System.Windows.Forms.Button btnChangeUser;
        private System.Windows.Forms.TextBox tbMessage;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.PictureBox picboxImage;
        private System.Windows.Forms.Label labelCmdList;
        private System.Windows.Forms.TextBox tbCommandList;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnSave;
        private IPAddressControlLib.IPAddressControl ipFam;
        private System.Windows.Forms.CheckBox checkPIV;
    }
}

