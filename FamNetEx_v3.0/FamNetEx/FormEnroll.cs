using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace FamNetEx
{
    public partial class FormEnroll : Form
    {
        public FormEnroll()
        {
            InitializeComponent();
            comboFingerID.SelectedIndex = 6;
            comboUserType.SelectedIndex = 0;
            comboSecurityLevel.SelectedIndex = 2;
        }

        public byte GroupID
        {
            get { return byte.Parse(textGroupID.Text); }
        }

        public string UserID
        {
            get { return textUserID.Text; }
        }

        public byte FingerID
        {
            get { return (byte)comboFingerID.SelectedIndex; }
        }

        public byte UserType
        {
            get 
            {
                byte nType = (byte)comboSecurityLevel.SelectedIndex;
                if (comboUserType.SelectedIndex == 0)   //VIP
                    nType += 0x04;
                return nType;
            }
        }

         private void btnOK_Click(object sender, EventArgs e)
        {
            if (textGroupID.Text.Length == 0 || textUserID.Text.Length == 0)
            {
                MessageBox.Show("Parameter is requied", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}